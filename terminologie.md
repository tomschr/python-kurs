# Python Terminologie

(Quelle: https://docs.python.org/3/glossary.html)

Argument
  Ein Wert das einer Funktion beim Aufruf übergeben wird.
  (keyword argument, positional argument)

Attribut
  Ein Wert das mit einem Objekt verknüpft ist. Es wird mit
  der Punkt-Syntax referenziert. Ein Objekt "O" mit einem
  Attribut "a" wird asl "O.a" referenziert.

Context Manager
  Ein Objekt das die Umgebung innerhalb eines with Blocks
  über `__enter__()` und `__exit__()` kontrolliert.

Decorator
  Eine Function die eine andere Funktion zurückliefert.

Dictionary
  Assoziatives Array, Hash, Schlüssel-Wert-Paar.

Docstring
  Ein String als erster Ausdruck einer Klasse, Funktion
  oder Modul zum Zwecke der Dokumentation.

EAFP
  Easier to ask for forgiveness than permission.

f-string
  String der ein `f` bzw. `F` vorangestellt ist ("formatted string")

Generator
  Eine Funktion die ein "Generator Iterator" zurückliefert. Ein
  Generator enthält einen yield-Ausdruck der über eine for-Schleife
  oder next()-Funktion konsumiert wird.

Generator Iterator
  Ein Objekt das durch eine Generator-Funktion erstellt wurde.
  Jeder yield hebt zeitweilig die Ausführung auf und behält sich
  den aktuellen Stand. Wenn der Generator Iterator fortsetzt,
  wird die Ausführung an der Stelle weitergeführt, wo sie
  unterbrochen wurde.

GIL (Global Interpreter Lock)
  Mechanismus vom CPython-Interpreter der sicherstellt, dass
  immer nur ein Thread Python-Bytecode ausführt.

Iterable
  Ein Objekt das fähig ist sein Element der Reihe nach
  zurückzugeben.

Iterator
  Ein Objekt das einen Strom von Daten repräsentiert.

LBYL
  Look before you leap.

