#!/usr/bin/env python3

"""
 normalstring = "This string\ncontains newlines\n"
 rawstring = r"This is a\nraw string"

"""

import re


def find_the_dog(text):
    """Matches a dog from the beginning
    """
    return re.match(r'dog', text)


def find_the_dog_anywhere(text):
    """Mathes a dog from anywhere
    """
    return re.search(r'dog', text)


def find_all_dogs(text):
    """Find all dogs from text
    """
    return re.findall(r'dog', text)

# ----------------------------------------

def finddate(datetext):
    """Finds a date in a text. The date has to be the
       format "yyyy-mm-dd"
       Returns a tuple in the form: (year, month, day)
       or an empty tuple if nothing found

    >>> finddate("2017-01-12")
    ('2017', '01', '12')
    >>> finddate("nix")
    ()
    >>> finddate("bla 2020-12-12 bla")
    ('2020', '12', '12')
    """
    m = re.search(r"(\d\d\d\d)-(\d\d)-(\d\d)", datetext)
    if m:
        return m.groups()
    else:
        return ()


def finddate_dict(datetext):
    """Finds a date in a text. The date has to be the
       format "yyyy-mm-dd"

    >>> d = finddate_dict("2017-01-12")
    >>> d['year'], d['month'], d['day']
    ('2017', '01', '12')
    >>> finddate_dict("nix")
    {}
    """
    m = re.search(r"(?P<year>\d\d\d\d)-"
                  r"(?P<month>\d\d)-"
                  r"(?P<day>\d\d)",
                  datetext)
    if m:
        return m.groupdict()
    else:
        return {}


def findlength(text):
    """Finds a length unit in a text. A length unit has the
       format <VALUE><LENGTHUNIT>, for example, 10m, 12km etc.
       The following length units are supported:
       m, km, cm,

       >>> d = findlength("I'm 174cm tall")
       >>> d['value'], d['unit']
       ('174', 'cm')
    """
    m = re.search(r"(?P<value>\d+)(?P<unit>m|km|cm)", text)
    if m:
        return m.groupdict()
    else:
        return {}


def findalllengths(text):
    """Finds a length unit in a text. A length unit has the
       format <VALUE><LENGTHUNIT>, for example, 10m, 12km etc.
       The following length units are supported:
       m, km, cm,

       >>> findalllengths("I'm 174cm and Tux is 180cm")
       [('174', 'cm'), ('180', 'cm')]
    """
    return re.findall(r"(?P<value>\d+)(?P<unit>m|km|cm)",
                      text)


def findalllengths_iter(unittext):
    """Finds a length unit in a text. A length unit has the
       format <VALUE><LENGTHUNIT>, for example, 10m, 12km etc.
       The following length units are supported:
       m, km, cm,

       >>> r = findalllengths_iter("I'm 174cm and Tux is 180cm")
       >>> [m.groups() for m in r]
       [('174', 'cm'), ('180', 'cm')]
    """
    return re.finditer(r"(?P<value>\d+)(?P<unit>m|km|cm)",
                      unittext)


if __name__ == "__main__":
    import doctest
    doctest.testmod()
