#!/usr/bin/python3 -W error
#
# More examples, see
# https://wiki.python.org/moin/PythonDecoratorLibrary
#
# python3 -u -W "error::UserWarning::0" decorator-example1.py

from functools import wraps
import warnings

# warnings.simplefilter('error', UserWarning)


def tags(tag):
    def tags_decorator(func):
        def wrapper(text):
            return "<{0}>{1}</{0}>".format(tag, func(text))
        return wrapper
    return tags_decorator


def bold(func):
    def wrapper(text):
        return "<b>{0}</b>".format(func(text))
    return wrapper


def strong(func):
    warnings.warn("Call to deprecated function {}.".format(func.__name__),
                       # category=DeprecationWarning
                       )
    def wrapper(text):
        return "<strong>{0}</strong>".format(func(text))
    return wrapper


@bold
def say_hello(name):
    return "Hello {0}".format(name)


@strong
@bold
def say_hello2(name):
    return "Hello {0}".format(name)


@bold
@strong
def say_hello3(name):
    return "Hello {0}".format(name)


@tags('p')
def say_hello4(name):
    return "Hello {0}".format(name)


if __name__ == "__main__":
    print(say_hello("Tux"))
    print(say_hello2("Tux"))
    print(say_hello3("Tux"))
    print(say_hello4("Tux"))
    
