#!/usr/bin/python3
# kate: indent-mode Python; indent-width 4; replace-tabs true; replace-tabs-save true;

"""

"""

def prod(iterable, start=1):
    """Return the product of an iterable of numbers (NOT strings)

    >>> prod([1,2,3,4])
    24
    >>> prod([1,2,], 2)
    2
    """
    p = 1
    for i in iterable:
        p *= i
    return p*start


def mean(data):
    """Calculates the arithmetic mean from a list of numbers

    :param list data: list of numbers
    :rtype:           arithmetic mean

    >>> mean([1,2,3,4])
    2.5
    >>> mean([1])
    1.0
    >>> mean([])
    Traceback (most recent call last):
        ...
    ValueError: numbers list must not be empty
    >>>
    """
    try:
        return sum(data)/len(data)
    except ZeroDivisionError:
        raise ValueError("numbers list must not be empty")


def stddev(data):
    """Standard deviation from a list of numbers

    :param list data: list of numbers
    :rtype:           standard devition

    >>> round(stddev([1,2]), 3)
    0.707
    >>> round(stddev([1,2,3,4]), 3)
    1.291
    """
    from math import sqrt
    xq = mean(data)
    n = len(data) -1

    return sqrt( sum([ (i - xq)**2 for i in data ])/n )


def fahrenheit2celsius(fahrenheit):
    """Convert Fahrenheit into Celsius

    :param number fahrenheit:  string or number of
    :rtype:                    Converted value to Celsius

    >>> fahrenheit2celsius(32)
    0.0
    >>> fahrenheit2celsius("32")
    0.0
    >>> round(fahrenheit2celsius(212), 3)
    100.0
    >>> round(fahrenheit2celsius(0), 3)
    -17.778
    >>> round(fahrenheit2celsius(10), 3)
    -12.222
    >>> round(fahrenheit2celsius(50), 3)
    10.0
    """
    return (float(fahrenheit) - 32) * 5/9


def mymax(a, b, *numbers):
    """Returns the maximum values from the arguments

    :param number a:      first number
    :param number b:      second number
    :param tuple numbers: a tuple containing additional numbers
    :rtype:               maximum value

    >>> mymax(1, 2)
    2
    >>> mymax(4, -2, 5, 10, -2, 0)
    10
    >>> mymin(1, 1)
    1
    """
    # return sorted( (a,b)+numbers )[-1]
    res=a
    for i in (b,)+ numbers:
        if i>res:
            res = i
    return res


def mymin(a, b, *numbers):
    """Returns the minimum values from the arguments

    :param number a:      first number
    :param number b:      second number
    :param tuple numbers: a tuple containing additional numbers
    :rtype:               minimum value

    >>> mymin(1, 2)
    1
    >>> mymin(4, -2, 5, 10, -2, 0)
    -2
    >>> mymin(1, 1)
    1
    """
    #return sorted( (a,b)+numbers )[0]
    res=a
    for i in (b,)+ numbers:
        if i<res:
            res = i
    return res



def splitatequal(line):
    """Splits lines between '=' and returns a dictionary { LEFT : RIGHT }

    :param str line: string containing key and value, separated by `=`
    :rtype:          dictionary of left and right string

    >>> splitatequal("min=24F")
    {'min': '24F'}
    >>> splitatequal("min = 24F")
    {'min': '24F'}
    >>> splitatequal("min =\t24F")
    {'min': '24F'}
    >>> splitatequal("\tmin =24F")
    {'min': '24F'}
    >>> splitatequal("\t min =24F")
    {'min': '24F'}
    >>> splitatequal("\t min =   24F\t\t")
    {'min': '24F'}
    >>> splitatequal("\t min =   24F \t  ")
    {'min': '24F'}
    """
    import re
    line = re.sub("[\t\n ]", "", line)
    return dict([line.split("=")])


def hline(numbers):
    """
    """
    import shutil
    size = shutil.get_terminal_size((80, 20))
    width = size.columns

    xlow, xhi = min(numbers), max(numbers)
    xscale = width / (xhi - xlow)
    xoffset = int(abs(xscale * xlow))





## -----------------------------------------------------------------------
def _test():
    import doctest
    #import sys
    #verbose = "-v" in sys.argv
    #for mod in modules:
        #doctest.testmod(mod, verbose=verbose, report=0)
    #doctest.master.summarize()
    doctest.testmod()

if __name__ == "__main__":
    _test()

# EOF