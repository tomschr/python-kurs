#!/usr/bin/python

import sys
from string import atoi, join

class BaseVersion:
  def __repr__ (self):
    return "%s ('%s')" % (self.__class__.__name__, str(self))


class Version(BaseVersion):
  """A version class to compare versions
  """

  def __init__(self, version):
    self.__setversion__(version)
  
  def __setversion__(self, version):
    assert type(version) in (type(""), type(u"")), "Expected string or unicode string"
    self._version = version.split(".")
    self._version = tuple(map(atoi, self._version))
  
  #def __repr__(self):
  #  return "%s ('%s')" % (self.__class__.__name__,  str(self) )
 
  def __str__(self):
    #return ".".join(self._version)
    return join(map(str, self._version), '.')
 
  def __lt__(self, other):
    return self._version < other
  
  def __gt__(self, other):
    return self._version > other
  
  def __eq__(self, other):
    return self._version == other
  
  @property
  def version(self):
    return str(self)
  
  @version.setter
  def version(self, value):
    self.__setversion__(value)
  
  @property
  def major(self):
    return self._version[0]
  
  @property
  def minor(self):
    return self._version[1]
  
  @property
  def patch(self):
    return self._version[2]
  
  

  
if __name__ == "__main__":
  print("--- %s ---" % sys.argv[0])
  v1 = Version("1.5.7")
  v2 = Version("1.5.8")
  
  print(repr(v1))
  print(v2)
  print("v1 < v2 %s" % bool(v1 < v2) )
  print("v1 > v2 %s" % bool(v1 > v2) )
  print("v1 == v2 %s" % bool(v1 == v2) )
  print("v1 <= v2 %s" % bool(v1 <= v2) )
  print("v1 = %s" % v1.version)
  v1.version = "2.5.6"
  print("v1 = %s" % v1.version)
  print("v1.major = %s" % v1.major)
  print("v1.minor = %s" % v1.minor)
  print("v1.patch = %s" % v1.patch)