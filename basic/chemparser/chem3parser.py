#!/usr/bin/python3

"""
BNF for simple chemical formula (no nesting)

 INTEGER ::= '0'..'9'+
 ELEMENT ::= 'A'..'Z' 'a'..'z'*
 TERM ::= ELEMENT [INTEGER]
 FORMULA ::= TERM+
"""

from atoms import ATOMS, Element
from itertools import tee, zip_longest
import re


class UnknownElement(ValueError):
    def __str__(self):
        # return "{}: {}".format(self.__class__.__name__, self.args[0])
        return "Unknown Element %r" % self.args[0]


def pairwise(iterable):
    "s -> (s0,s1), (s1,s2), (s2, s3), ..."
    iterable, copy = tee(iterable)

    next(copy, None)
    for token in iterable:
        nexttoken = next(copy, None)
        if isinstance(token, Element) and isinstance(nexttoken, int):
            yield (token, nexttoken)
        elif isinstance(token, Element) and isinstance(nexttoken, (Element, type(None))):
            yield (token, 1)


def chemlexer(formula):
    """Returns a tokenized formula

    :param str formula: the chemical formula as a string
    :raises: :class:`UnknownElement`
    :return: a parsed representation of the formula; each element is represented
             in a tuple as tuple(str, int)
    :rtype: list(tuple(str, int))

    >>> list(chemlexer("H2O"))
    [(Element(symbol='H', z=1, weight=1.008), 2), (Element(symbol='O', z=8, weight=15.999), 1)]

    >>> list(chemlexer("H2SO4"))
    [(Element(symbol='H', z=1, weight=1.008), 2), (Element(symbol='S', z=16, weight=32.06), 1), (Element(symbol='O', z=8, weight=15.999), 4)]
    
    >>> list(chemlexer("X4"))
    Traceback (most recent call last):
    ...
    chem3parser.UnknownElement: Unknown Element 'X'
    """
    def element(scanner, token):
        # print(" element scanner:", scanner.match)
        if token not in ATOMS:
            raise UnknownElement(token)
        return ATOMS[token]

    def integer(scanner, token):
        # print(" integer scanner:", scanner.match)
        return int(token)

    # http://stackoverflow.com/a/4136323
    scanner = re.Scanner([
         (r'(?P<element>[A-Z][a-z]{0,2})', element),
         (r'(?P<amount>\d+)?',         integer),
         (r" +",                       lambda scanner, token: None),
        ])

    yield from pairwise(scanner.scan(formula)[0])


def atomicweight(formula):
    """Calculates the atomic weight of a given formula

    :param str formula: the chemical formula as a string
    :return: the atomic weight

    >>> atomicweight("H2O")
    18.015

    >>> atomicweight("C2H5OH")
    46.069

    >>> atomicweight("NaCl")
    58.43976928000001

    >>> atomicweight("CNaNCl")
    84.45776928000001
    """
    return sum([e.weight*v for e, v in chemlexer(formula)])


def test(formulas):
    if not formulas:
        formulas = ["H2O", "C2H5OH", "H2SO4"]
    for f in formulas:
        try:
            lex = chemlexer(f)
            print(f, '->', list(lex), "=>", atomicweight(f))
        except UnknownElement as err:
            print("Error: %s" % err, file=sys.stderr)


if __name__ == "__main__":
    import sys
    test(sys.argv[1:])
