#!/usr/bin/python3
"""
BNF for simple chemical formula (no nesting)

 INTEGER ::= '0'..'9'+
 ELEMENT ::= 'A'..'Z' 'a'..'z'*
 TERM ::= ELEMENT [INTEGER]
 FORMULA ::= TERM+

 Lexer/Tokenizer = Programm zur Zerlegung von Text in Folgen
                   von logisch zusammengehörigen Einheiten,
                   sog. Token.

"""

from atoms import ATOMS, JOINED_ATOMS
import re
import sys

ATOMRE = re.compile(r"(?P<atom>{})"
                     "(?P<amount>\d+)?".format(JOINED_ATOMS))


def chemlexer(formula):
    """Returns a generator of (symbol, amount) tuples of a given formula
       otherwise an empty list

    :param str formula: the chemical formula as a string
    :return: a parsed representation of the formula; each element is represented
             in a tuple as tuple(str, int)
    :rtype: list(tuple(str, int))

    # Wasser
    >>> list(chemlexer("H2O"))
    [('H', 2), ('O', 1)]

    # Natriumethanolat
    >>> list(chemlexer("C2H5ONa"))
    [('C', 2), ('H', 5), ('O', 1), ('Na', 1)]

    # Schwefelsäure
    >>> list(chemlexer("H2SO4"))
    [('H', 2), ('S', 1), ('O', 4)]

    >>> list(chemlexer("X"))
    []

    >>> list(chemlexer("a"))
    []
    """

    for token in ATOMRE.finditer(formula):
        atom = token.group('atom')     # token.group(0)
        amount = token.group('amount') # token.group(1)
        amount = 1 if amount is None else int(amount)
        yield (atom, amount)


def atomicweight(formula):
    """Calculates the atomic weight of a given formula

    :param str formula: the chemical formula as a string
    :return: the atomic weight

    >>> atomicweight("H2O")
    18.015

    >>> atomicweight("C2H5OH")
    46.069

    >>> atomicweight("NaCl")
    58.43976928000001

    >>> atomicweight("CNaNCl")
    84.45776928000001
    """
    summa = 0.0
    for atom, amount in chemlexer(formula):
        element = ATOMS[atom]
        summa += element.weight * amount
    return summa

    # One liner:
    # return sum([(ATOMS[e].weight)*v for e, v in chemlexer(formula)])


def test(formulas):
    if not formulas:
        formulas = ["H2O",
                    "C2H5OH",
                    "C21H22N2O2", # Strychnin
                    "C6H5ClO",    # Chlorphenol
                    ]
    for f in formulas:
        print("{} -> {} -> {}".format(f,
                                      list(chemlexer(f)),
                                      atomicweight(f)))


if __name__ == "__main__":
    test(sys.argv[1:])
    sys.exit(0)
