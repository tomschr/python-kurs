#!/usr/bin/python3

"""
BNF for nested chemical formula

  FORMULA := TERM+
  TERM    := (ELEMENT | '(' FORMULA ')') [INTEGER]
  ELEMENT := 'A'..'Z' 'a'..'z'*
  INTEGER := '0'..'9'+
"""

from collections import OrderedDict
import re

from atoms import ATOMS, Element


class FormulaError(ValueError):
    """Base class for all chemical related exceptions"""
    pass


class UnknownElementError(FormulaError):
    """Exception for all unknown elements"""
    def __str__(self):
        # return "{}: {}".format(self.__class__.__name__, self.args[0])
        return "Unknown Element %r" % self.args[0]


class ParenthesisMismatchError(FormulaError):
    """Exception for all problems with parenthesis"""
    def __str__(self):
        return self.args[0]


def extract(s):
    """Create substring between parenthesis

    :param str s: String with balanced parenthesis
    :return: match object or None
    :rtype: :class:`_sre.SRE_Match` | None
    """
    match = re.search(r'\((.*)\)', s)
    return match


def chemlexer(formula):
    """Returns a tokenized formula

    :param str formula: the chemical formula as a string
    :raises: :class:`UnknownElementError`, :class:`ParenthesisMismatchError`
    :return: a parsed representation of the formula; each element is represented
             in a tuple as tuple(str, int)
    :rtype: list(tuple(str, int))

    >>> list(chemlexer("H2O"))
    [(Element(symbol='H', z=1, weight=1.008), 2), (Element(symbol='O', z=8, weight=15.999), 1)]

    >>> list(chemlexer("H2SO4"))
    [(Element(symbol='H', z=1, weight=1.008), 2), (Element(symbol='S', z=16, weight=32.06), 1), (Element(symbol='O', z=8, weight=15.999), 4)]

    >>> list(chemlexer("X4"))
    Traceback (most recent call last):
    ...
    chem5parser.UnknownElementError: Unknown Element 'X'
    """
    # Check first:
    if not formula:
        raise FormulaError("empty formula", formula)
        yield []
    if formula.count('(') != formula.count(')'):
        raise ParenthesisMismatchError('parentheses mismatch')
    if formula.endswith('('):
        raise ParenthesisMismatchError('formula cannot end with open parenthesis')

    VALIDCHARS = set("(123456789ABCDEFGHIKLMNOPRSTUVWXYZ")
    if not formula[0] in VALIDCHARS:
        raise FormulaError("unexpected character %r" % formula[0], formula)

    VALIDCHARS |= set(")0abcdefghiklmnoprstuy")

    elements = OrderedDict()
    OPENPAR = '('
    CLOSEPAR = ')'
    ele = ''         # parsed element
    num = 0          # number
    level = 0        # parenthesis level
    counts = [1]     # parenthesis level multiplication
    i = len(formula) # index
    while i:
        i -= 1
        char = formula[i]
        if char not in VALIDCHARS:
            raise FormulaError(
                "unexpected character %r" % char, formula)
        if char in OPENPAR:
            level -= 1
            if level < 0 or num != 0:
                raise FormulaError(
                    "missing closing parenthesis ')'", formula)
        elif char in CLOSEPAR:
            if num == 0:
                    num = 1
            level += 1
            if level > len(counts) - 1:
                counts.append(0)
            counts[level] = num * counts[level - 1]
            num = 0
        elif char.isdigit():
            j = i
            while i and formula[i - 1].isdigit():
                i -= 1
            num = int(formula[i:j + 1])
            if num == 0:
                raise FormulaError("count is zero", formula)
        elif char.islower():
            if not formula[i - 1].isupper():
                raise FormulaError(
                    "unexpected character '%s'" % char, formula, i)
            ele = char
        elif char.isupper():
            ele = char + ele
            if num == 0:
                num = 1
            if ele not in ATOMS:
                raise FormulaError("unknown symbol %r" % ele, formula)
            number = num * counts[level]
            if ele not in elements:
                print(">#>", ele, num, number)
                elements[ele] = number
            ele = ''
            num = 0

    if num != 0:
        raise FormulaError("number preceding formula", formula)

    if level != 0:
        raise FormulaError(
            "missing opening parenthesis '('", formula)

    if not elements:
        raise FormulaError("invalid formula", formula)

    yield from reversed(elements.items())

    # https://stackoverflow.com/questions/13923325/parsing-chemical-formula#13923354
    # for symbol, integer in re.findall(r'([A-Z][a-z]*)(\d*)', formula):
    #    if symbol not in ATOMS:
    #        raise UnknownElementError(symbol)
    #    integer = 1 if not integer else int(integer)
    #    yield ATOMS[symbol], integer


def atomicweight(formula):
    """Calculates the atomic weight of a given formula

    :param str formula: the chemical formula as a string
    :return: the atomic weight

    >>> atomicweight("H2O")
    18.015

    >>> atomicweight("C2H5OH")
    46.069

    >>> atomicweight("NaCl")
    58.43976928000001

    >>> atomicweight("CNaNCl")
    84.45776928000001
    """
    return sum([e.weight*v for e, v in chemlexer(formula)])


def test(formulas):
    if not formulas:
        formulas = ["H2O", "C2H5OH", "H2SO4", "(CH3)2CC(CH3)2"]
    for f in formulas:
        lex = chemlexer(f)
        print(f, '->', list(lex))
    #    try:
    #        lex = chemlexer(f)
    #        print(f, '->', list(lex), "=>", atomicweight(f))
    #    except UnknownElementError as err:
    #        print("Error: %s" % err, file=sys.stderr)


if __name__ == "__main__":
    import sys
    test(sys.argv[1:])
