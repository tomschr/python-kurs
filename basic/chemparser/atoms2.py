#!/usr/bin/python3
"""Represents an atom in a molecule

"""

__all__ = ("Atom", "ATOMS", "ALLATOMS")

from functools import total_ordering as _total_ordering
import re as _re


@_total_ordering
class Atom(object):
    """An atom with a symbol, an atomic number, an atomic weight, and a name
    """
    REGEX = _re.compile(r"(%[szwn%])")
    STDFORMAT = "symbol=%s, z=%z, weight=%w, name=%n"

    def __init__(self, symbol, z, weight, name):
        self.symbol = symbol
        self.z = z
        self.weight = weight
        self.name = name

    def __eq__(self, other):
        return self.z == other.z
    def __lt__(self, other):
        return self.z < other.z

    def __str__(self):
        """str(self)"""
        return "{}({})".format(type(self).__name__,
                               format(self, self.STDFORMAT)
                               )

    def __repr__(self):
        """repr(self)"""
        return "{}".format(self.symbol)

    # PEP 3101 support:
    def __format__(self, fmt=''):
        """Formats a Atom instance

        param str fmt: the format string (default is '')
          %s = symbol of the atom
          %z = atomic number
          %w = atomic weight
          %n = name of the atom
          %% = the percent symbol

        Examples:
        >>> H = Atom('H', 1, 1.008, "Hydrogen")
        >>> "{:}".format(H)
        'Atom(symbol=H, z=1, weight=1.008, name=Hydrogen)'
        >>> "{:%n}".format(H)
        'Hydrogen'
        >>> '{:%z}'.format(H)
        '1'
        >>> '{:%s}'.format(H)
        'H'
        >>> '{:%w}'.format(H)
        '1.008'
        >>> '{:%n, %z: %w}'.format(H)
        'Hydrogen, 1: 1.008'

        :return: formatted string
        :rtype: str
        """
        # Check first if we have a string instance:
        #if not isinstance(fmt, str):
        #    raise TypeError("must be str, not %s" % type(fmt).__name__)

        def matcher(match):
            "Replace match object's result with corresponding value"
            result = {'%s': self.symbol,
                      '%n': self.name,
                      '%z': str(self.z),
                      '%w': str(self.weight),
                      '%%': '%',
                      }
            mm = match.group(0)
            return result.get(mm, mm)

        # If fmt contains something, then format it,
        # otherwise use default formatting with str():
        if not fmt:
            return str(self)
        return self.REGEX.sub(matcher, fmt)

    def __mul__(self, other):
        if not isinstance(other, int):
            return NotImplemented
        return self.weight * other

    def __rmul__(self, other):
        return self.__mul__(other)

    def __add__(self, other):
        if isinstance(other, (float, int)):
            return self.weight + other
        if isinstance(other, Atom):
            return self.weight + other.weight
        return NotImplemented

    def __radd__(self, other):
        return self.__add__(other)


ATOMS = {
  'H':   Atom('H',   1, 1.008, 'Wasserstoff'),
  'D':   Atom('D',   1, 2.0141, 'Deuterium'),
  'He':  Atom('He',  2, 4.002602, 'Helium'),
  'Li':  Atom('Li',  3, 6.94, 'Lithium'),
  'Be':  Atom('Be',  4, 9.0121831, 'Beryllium'),
  'B':   Atom('B',   5, 10.81, 'Bor'),
  'C':   Atom('C',   6, 12.011, 'Kohlenstoff'),
  'N':   Atom('N',   7, 14.007, 'Stickstoff'),
  'O':   Atom('O',   8, 15.999, 'Sauerstoff'),
  'F':   Atom('F',   9, 18.998403163, 'Fluor'),
  'Ne':  Atom('Ne', 10, 20.1797, 'Neon'),
  'Na':  Atom('Na', 11, 22.98976928, 'Natrium'),
  'Mg':  Atom('Mg', 12, 24.305, 'Magnesium'),
  'Al':  Atom('Al', 13, 26.9815385, 'Aluminium'),
  'Si':  Atom('Si', 14, 28.085, 'Silicum'),
  'P':   Atom('P',  15, 30.973761998, 'Phosphor'),
  'S':   Atom('S',  16, 32.06, 'Schwefel'),
  'Cl':  Atom('Cl', 17, 35.45, 'Chlor'),
  'Ar':  Atom('Ar', 18, 39.948, 'Argon'),
  'K':   Atom('K',  19, 39.0983, 'Kalium'),
  'Ca':  Atom('Ca', 20, 40.078, 'Calcium'),
  'Sc':  Atom('Sc', 21, 44.955908, 'Scandium'),
  'Ti':  Atom('Ti', 22, 47.867, 'Titan'),
  'V':   Atom('V',  23, 50.9415, 'Vanadium'),
  'Cr':  Atom('Cr', 24, 51.9961, 'Chrom'),
  'Mn':  Atom('Mn', 25, 54.938044, 'Mangan'),
  'Fe':  Atom('Fe', 26, 55.845, 'Eisen'),
  'Co':  Atom('Co', 27, 58.933194, 'Cobalt'),
  'Ni':  Atom('Ni', 28, 58.6934, 'Nickel'),
  'Cu':  Atom('Cu', 29, 63.546, 'Kupfer'),
  'Zn':  Atom('Zn', 30, 65.38, 'Zink'),
  'Ga':  Atom('Ga', 31, 69.723, 'Gallium'),
  'Ge':  Atom('Ge', 32, 72.630, 'Germanium'),
  'As':  Atom('As', 33, 74.921595, 'Arsen'),
  'Se':  Atom('Se', 34, 78.971, 'Selen'),
  'Br':  Atom('Br', 35, 79.904, 'Brom'),
  'Kr':  Atom('Kr', 36, 83.798, 'Krypton'),
  'Rb':  Atom('Rb', 37, 85.4678, 'Rubidium'),
  'Sr':  Atom('Sr', 38, 87.62, 'Strontium'),
  'Y':   Atom('Y',  39, 88.90584, 'Yttrium'),
  'Zr':  Atom('Zr', 40, 91.224, 'Zirconium'),
  'Nb':  Atom('Nb', 41, 92.90637, 'Niob'),
  'Mo':  Atom('Mo', 42, 95.95, 'Molybdän'),
  'Tc':  Atom('Tc', 43, 98, 'Technicium'),
  'Ru':  Atom('Ru', 44, 101.07, 'Ruthenium'),
  'Rh':  Atom('Rh', 45, 102.90550, 'Rhodium'),
  'Pd':  Atom('Pd', 46, 106.42, 'Palladium'),
  'Ag':  Atom('Ag', 47, 107.8682, 'Silber'),
  'Cd':  Atom('Cd', 48, 112.414, 'Cadmium'),
  'In':  Atom('In', 49, 114.818, 'Indium'),
  'Sn':  Atom('Sn', 50, 118.710, 'Zinn'),
  'Sb':  Atom('Sb', 51, 121.760, 'Antimon'),
  'Te':  Atom('Te', 52, 127.60, 'Tellur'),
  'I':   Atom('I',  53, 126.90447, 'Iod'),
  'J':   Atom('J',  53, 126.90447, 'Jod'),
  'Xe':  Atom('Xe', 54, 131.293, 'Xenon'),
  'Cs':  Atom('Cs', 55, 132.90545196, 'Caesium'),
  'Ba':  Atom('Ba', 56, 137.327, 'Barium'),
  'La':  Atom('La', 57, 138.90547, 'Lanthan'),
  'Ce':  Atom('Ce', 58, 140.116, 'Cer'),
  'Pr':  Atom('Pr', 59, 140.90766, 'Praseodym'),
  'Nd':  Atom('Nd', 60, 144.242, 'Neodym'),
  'Pm':  Atom('Pm', 61, 145, 'Prometium'),
  'Sm':  Atom('Sm', 62, 150.36, 'Samarium'),
  'Eu':  Atom('Eu', 63, 151.964, 'Europhium'),
  'Gd':  Atom('Gd', 64, 157.25, 'Gadolinium'),
  'Tb':  Atom('Tb', 65, 158.92535, 'Terbium'),
  'Dy':  Atom('Dy', 66, 162.500, 'Dysprosium'),
  'Ho':  Atom('Ho', 67, 164.93033, 'Holium'),
  'Er':  Atom('Er', 68, 167.259, 'Erbium'),
  'Tm':  Atom('Tm', 69, 168.93422, 'Thulium'),
  'Yb':  Atom('Yb', 70, 173.045, 'Ytterbium'),
  'Lu':  Atom('Lu', 71, 174.9668, 'Lutetium'),
  'Hf':  Atom('Hf', 72, 178.49, 'Hafnium'),
  'Ta':  Atom('Ta', 73, 180.94788, 'Tantal'),
  'W':   Atom('W',  74, 183.84, 'Wolfram'),
  'Re':  Atom('Re', 75, 186.207, 'Rhenium'),
  'Os':  Atom('Os', 76, 190.23, 'Osmium'),
  'Ir':  Atom('Ir', 77, 192.217, 'Iridium'),
  'Pt':  Atom('Pt', 78, 195.084, 'Platin'),
  'Au':  Atom('Au', 79, 196.966569, 'Gold'),
  'Hg':  Atom('Hg', 80, 200.592, 'Quecksilber'),
  'Tl':  Atom('Tl', 81, 204.38, 'Thallium'),
  'Pb':  Atom('Pb', 82, 207.2, 'Blei'),
  'Bi':  Atom('Bi', 83, 208.98040, 'Bismut'),
  'Po':  Atom('Po', 84, 209, 'Polonium'),
  'At':  Atom('At', 85, 210, 'Astat'),
  'Rn':  Atom('Rn', 86, 222, 'Radon'),
  'Fr':  Atom('Fr', 87, 223, 'Francium'),
  'Ra':  Atom('Ra', 88, 226, 'Radium'),
  'Ac':  Atom('Ac', 89, 227, 'Actinium'),
  'Th':  Atom('Th', 90, 232.0377, 'Thorium'),
  'Pa':  Atom('Pa', 91, 231.03588, 'Protactinium'),
  'U':   Atom('U',  92, 238.02891, 'Uran'),
  'Np':  Atom('Np', 93, 237, 'Neptunium'),
  'Pu':  Atom('Pu', 94, 244, 'Plutonium'),
  'Am':  Atom('Am', 95, 243, 'Americium'),
  'Cm':  Atom('Cm', 96, 247, 'Curium'),
  'Bk':  Atom('Bk', 97, 247, 'Berkelium'),
  'Cf':  Atom('Cf', 98, 251, 'Californium'),
  'Es':  Atom('Es', 99, 252, 'Einsteinium'),
  'Fm':  Atom('Fm', 100, 257, 'Fermium'),
  'Md':  Atom('Md', 101, 258, 'Mendelevium'),
  'No':  Atom('No', 102, 259, 'Nobelium'),
  'Lr':  Atom('Lr', 103, 266, 'Lawrencium'),
  'Rf':  Atom('Rf', 104, 267, 'Rutherfordium'),
  'Db':  Atom('Db', 105, 268, 'Dubnium'),
  'Sg':  Atom('Sg', 106, 269, 'Seaborgium'),
  'Bh':  Atom('Bh', 107, 270, 'Bohrium'),
  'Hs':  Atom('Hs', 108, 269, 'Hassium'),
  'Mt':  Atom('Mt', 109, 278, 'Meitnerium'),
  'Ds':  Atom('Ds', 110, 281, 'Darmstadtium'),
  'Rg':  Atom('Rg', 111, 281, 'Roentgenium'),
  'Cn':  Atom('Cn', 112, 285, 'Copernicium'),
  'Nh':  Atom('Nh', 113, 286, 'Nihonium'),     # /Ununtrium
  'Fl':  Atom('Fl', 114, 289, 'Flerovium'),    # /Ununquadium
  'Mc':  Atom('Mc', 115, 289, 'Moscovium'),    # /Ununpentium
  'Lv':  Atom('Lv', 116, 293, 'Livermorium'),  # /Ununhexium
  'Ts':  Atom('Ts', 117, 294, 'Tennessine'),   # /Ununseptium
  'Og':  Atom('Og', 118, 294, 'Oganesson'),    # /Ununoctium
}

ALLATOMS = [a for a in ATOMS]
# Sort after string length
ALLATOMS.sort(key=len, reverse=True)

if __name__ == "__main__":
    import doctest
    doctest.testmod()
