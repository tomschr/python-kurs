#!/usr/bin/python3

"""
BNF for simple chemical formula (no nesting)

 INTEGER ::= '0'..'9'+
 ELEMENT ::= 'A'..'Z' 'a'..'z'*
 TERM ::= ELEMENT [INTEGER]
 FORMULA ::= TERM+
"""

# Further tasks:
#
# * Implement the Hill notation system,
#   see https://en.wikipedia.org/wiki/Chemical_formula#Hill_system
# * Use "real" subnumbers from Unicode U+208X (where 1 <= X <= 9)
#   to make chemical formulas more pretty
# * Create a special class which implements __format__ and other
#   magic methods


from atoms import ATOMS, Element
import re


class UnknownElement(ValueError):
    def __str__(self):
        # return "{}: {}".format(self.__class__.__name__, self.args[0])
        return "Unknown Element %r" % self.args[0]


def chemlexer(formula):
    """Returns a tokenized formula

    :param str formula: the chemical formula as a string
    :raises: :class:`UnknownElement`
    :return: a parsed representation of the formula; each element is represented
             in a tuple as tuple(str, int)
    :rtype: list(tuple(str, int))

    >>> list(chemlexer("H2O"))
    [(Element(symbol='H', z=1, weight=1.008), 2), (Element(symbol='O', z=8, weight=15.999), 1)]

    >>> list(chemlexer("H2SO4"))
    [(Element(symbol='H', z=1, weight=1.008), 2), (Element(symbol='S', z=16, weight=32.06), 1), (Element(symbol='O', z=8, weight=15.999), 4)]
    
    >>> list(chemlexer("X4"))
    Traceback (most recent call last):
    ...
    chem4parser.UnknownElement: Unknown Element 'X'
    """
    # https://stackoverflow.com/questions/13923325/parsing-chemical-formula#13923354
    for symbol, integer in re.findall(r'([A-Z][a-z]*)(\d*)', formula):
        if symbol not in ATOMS:
            raise UnknownElement(symbol)
        integer = 1 if not integer else int(integer)
        yield ATOMS[symbol], integer


def atomicweight(formula):
    """Calculates the atomic weight of a given formula

    :param str formula: the chemical formula as a string
    :return: the atomic weight

    >>> atomicweight("H2O")
    18.015

    >>> atomicweight("C2H5OH")
    46.069

    >>> atomicweight("NaCl")
    58.43976928000001

    >>> atomicweight("CNaNCl")
    84.45776928000001
    """
    return sum([e.weight*v for e, v in chemlexer(formula)])


def test(formulas):
    if not formulas:
        formulas = ["H2O", "C2H5OH", "H2SO4"]
    for f in formulas:
        try:
            lex = chemlexer(f)
            print(f, '->', list(lex), "=>", atomicweight(f))
        except UnknownElement as err:
            print("Error: %s" % err, file=sys.stderr)


if __name__ == "__main__":
    import sys
    test(sys.argv[1:])
