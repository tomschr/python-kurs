#!/usr/bin/python3

from collections import namedtuple

Element = namedtuple('Element', 'symbol, z, weight')


#class Element(object):
#    def __init__(self, symbol, z, weight, name=None):
#        self.symbol = symbol
#        self.z = z
#        self.weight = weight
#        self.name = name
#
#    def __eq__(self, other):
#        return self.z == other.z
#    def __lt__(self, other):
#        return self.z < other.z
#    def __gt__(self, other):
#        return self.z > other.z
#    def __le__(self, other):
#        return self.z <= other.z
#    def __ge__(self, other):
#        return self.z >= other.z
#    def __ne__(self, other):
#        return self.z != other.z
#
#    def __repr__(self):
#        return "%s(%s, %s, %s, %s)" % (type(self).__name__,
#                                   self.symbol, self.z, self.weight, self.name)
#
#    def __str__(self):
#        return "%s" % self.symbol

ATOMS = {
  'H':   Element('H',   1, 1.008),          # Wasserstoff
  'D':   Element('D',   1, 2.0141),         # Deuterium
  'He':  Element('He',  2, 4.002602),       # Helium
  'Li':  Element('Li',  3, 6.94),           # Lithium
  'Be':  Element('Be',  4, 9.0121831),      # Beryllium
  'B':   Element('B',   5, 10.81),          # Bor
  'C':   Element('C',   6, 12.011),         # Kohlenstoff
  'N':   Element('N',   7, 14.007),         # Stickstoff
  'O':   Element('O',   8, 15.999),         # Sauerstoff
  'F':   Element('F',   9, 18.998403163),   # Fluor
  'Ne':  Element('Ne', 10, 20.1797),        # Neon
  'Na':  Element('Na', 11, 22.98976928),    # Natrium
  'Mg':  Element('Mg', 12, 24.305),         # Magnesium
  'Al':  Element('Al', 13, 26.9815385),     # Aluminium
  'Si':  Element('Si', 14, 28.085),         # Silicum
  'P':   Element('P',  15, 30.973761998),   # Phosphor
  'S':   Element('S',  16, 32.06),          # Schwefel
  'Cl':  Element('Cl', 17, 35.45),          # Chlor
  'Ar':  Element('Ar', 18, 39.948),         # Argon
  'K':   Element('K',  19, 39.0983),        # Kalium
  'Ca':  Element('Ca', 20, 40.078),         # Calcium
  'Sc':  Element('Sc', 21, 44.955908),      # Scandium
  'Ti':  Element('Ti', 22, 47.867),         # Titan
  'V':   Element('V',  23, 50.9415),        # Vanadium
  'Cr':  Element('Cr', 24, 51.9961),        # Chrom
  'Mn':  Element('Mn', 25, 54.938044),      # Mangan
  'Fe':  Element('Fe', 26, 55.845),         # Eisen
  'Co':  Element('Co', 27, 58.933194),      # Cobalt
  'Ni':  Element('Ni', 28, 58.6934),        # Nickel
  'Cu':  Element('Cu', 29, 63.546),         # Kupfer
  'Zn':  Element('Zn', 30, 65.38),          # Zink
  'Ga':  Element('Ga', 31, 69.723),         # Gallium
  'Ge':  Element('Ge', 32, 72.630),         # Germanium
  'As':  Element('As', 33, 74.921595),      # Arsen
  'Se':  Element('Se', 34, 78.971),         # Selen
  'Br':  Element('Br', 35, 79.904),         # Brom
  'Kr':  Element('Kr', 36, 83.798),         # Krypton
  'Rb':  Element('Rb', 37, 85.4678),        # Rubidium
  'Sr':  Element('Sr', 38, 87.62),          # Strontium
  'Y':   Element('Y',  39, 88.90584),       # Yttrium
  'Zr':  Element('Zr', 40, 91.224),         # Zirconium
  'Nb':  Element('Nb', 41, 92.90637),       # Niob
  'Mo':  Element('Mo', 42, 95.95),          # Molybdän
  'Tc':  Element('Tc', 43, 98),             # Technicium
  'Ru':  Element('Ru', 44, 101.07),         # Ruthenium
  'Rh':  Element('Rh', 45, 102.90550),      # Rhodium
  'Pd':  Element('Pd', 46, 106.42),         # Palladium
  'Ag':  Element('Ag', 47, 107.8682),       # Silber
  'Cd':  Element('Cd', 48, 112.414),        # Cadmium
  'In':  Element('In', 49, 114.818),        # Indium
  'Sn':  Element('Sn', 50, 118.710),        # Zinn
  'Sb':  Element('Sb', 51, 121.760),        # Antimon
  'Te':  Element('Te', 52, 127.60),         # Tellur
  'I':   Element('I',  53, 126.90447),      # Iod
  'J':   Element('J',  53, 126.90447),      # Jod
  'Xe':  Element('Xe', 54, 131.293),        # Xenon
  'Cs':  Element('Cs', 55, 132.90545196),   # Caesium
  'Ba':  Element('Ba', 56, 137.327),        # Barium
  'La':  Element('La', 57, 138.90547),      # Lanthan
  'Ce':  Element('Ce', 58, 140.116),        # Cer
  'Pr':  Element('Pr', 59, 140.90766),      # Praseodym
  'Nd':  Element('Nd', 60, 144.242),        # Neodym
  'Pm':  Element('Pm', 61, 145),            # Prometium
  'Sm':  Element('Sm', 62, 150.36),         # Samarium
  'Eu':  Element('Eu', 63, 151.964),        # Europhium
  'Gd':  Element('Gd', 64, 157.25),         # Gadolinium
  'Tb':  Element('Tb', 65, 158.92535),      # Terbium
  'Dy':  Element('Dy', 66, 162.500),        # Dysprosium
  'Ho':  Element('Ho', 67, 164.93033),      # Holium
  'Er':  Element('Er', 68, 167.259),        # Erbium
  'Tm':  Element('Tm', 69, 168.93422),      # Thulium
  'Yb':  Element('Yb', 70, 173.045),        # Ytterbium
  'Lu':  Element('Lu', 71, 174.9668),       # Lutetium
  'Hf':  Element('Hf', 72, 178.49),         # Hafnium
  'Ta':  Element('Ta', 73, 180.94788),      # Tantal
  'W':   Element('W',  74, 183.84),         # Wolfram
  'Re':  Element('Re', 75, 186.207),        # Rhenium
  'Os':  Element('Os', 76, 190.23),         # Osmium
  'Ir':  Element('Ir', 77, 192.217),        # Iridium
  'Pt':  Element('Pt', 78, 195.084),        # Platin
  'Au':  Element('Au', 79, 196.966569),     # Gold
  'Hg':  Element('Hg', 80, 200.592),        # Quecksilber
  'Tl':  Element('Tl', 81, 204.38),         # Thallium
  'Pb':  Element('Pb', 82, 207.2),          # Blei
  'Bi':  Element('Bi', 83, 208.98040),      # Bismut
  'Po':  Element('Po', 84, 209),            # Polonium
  'At':  Element('At', 85, 210),            # Astat
  'Rn':  Element('Rn', 86, 222),            # Radon
  'Fr':  Element('Fr', 87, 223),            # Francium
  'Ra':  Element('Ra', 88, 226),            # Radium
  'Ac':  Element('Ac', 89, 227),            # Actinium
  'Th':  Element('Th', 90, 232.0377),       # Thorium
  'Pa':  Element('Pa', 91, 231.03588),      # Protactinium
  'U':   Element('U',  92, 238.02891),      # Uran
  'Np':  Element('Np', 93, 237),            # Neptunium
  'Pu':  Element('Pu', 94, 244),            # Plutonium
  'Am':  Element('Am', 95, 243),            # Americium
  'Cm':  Element('Cm', 96, 247),            # Curium
  'Bk':  Element('Bk', 97, 247),            # Berkelium
  'Cf':  Element('Cf', 98, 251),            # Californium
  'Es':  Element('Es', 99, 252),            # Einsteinium
  'Fm':  Element('Fm', 100, 257),           # Fermium
  'Md':  Element('Md', 101, 258),           # Mendelevium
  'No':  Element('No', 102, 259),           # Nobelium
  'Lr':  Element('Lr', 103, 266),           # Lawrencium
  'Rf':  Element('Rf', 104, 267),           # Rutherfordium
  'Db':  Element('Db', 105, 268),           # Dubnium
  'Sg':  Element('Sg', 106, 269),           # Seaborgium
  'Bh':  Element('Bh', 107, 270),           # Bohrium
  'Hs':  Element('Hs', 108, 269),           # Hassium
  'Mt':  Element('Mt', 109, 278),           # Meitnerium
  'Ds':  Element('Ds', 110, 281),           # Darmstadtium
  'Rg':  Element('Rg', 111, 281),           # Roentgenium
  'Cn':  Element('Cn', 112, 285),           # Copernicium
  'Nh':  Element('Nh', 113, 286),           # Nihonium/Ununtrium
  'Fl':  Element('Fl', 114, 289),           # Flerovium/Ununquadium
  'Mc':  Element('Mc', 115, 289),           # Moscovium/Ununpentium
  'Lv':  Element('Lv', 116, 293),           # Livermorium/Ununhexium
  'Ts':  Element('Ts', 117, 294),           # Tennessine/Ununseptium
  'Og':  Element('Og', 118, 294),           # Oganesson/Ununoctium
}

ALLATOMS = [a for a in ATOMS]
# Sort after string length
ALLATOMS.sort(key=len, reverse=True)

# Needed for reg expressions
JOINED_ATOMS = "|".join(ALLATOMS)
