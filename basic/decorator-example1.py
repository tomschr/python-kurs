#!/usr/bin/python3 -W error
#
# More examples, see
# https://wiki.python.org/moin/PythonDecoratorLibrary
# https://pythonconquerstheuniverse.wordpress.com/2012/04/29/python-decorators/

def bold(func):
    def wrapper(text):
        return "<b>{0}</b>".format(func(text))
    return wrapper


def strong(func):
    def wrapper(text):
        return "<strong>{0}</strong>".format(func(text))
    return wrapper


@bold
def say_hello(name):
    return "Hello {0}".format(name)


@strong
@bold
def say_hello2(name):
    return "Hello {0}".format(name)


@bold
@strong
def say_hello3(name):
    return "Hello {0}".format(name)


if __name__ == "__main__":
    print(say_hello("Tux"))
    print(say_hello2("Tux"))
    print(say_hello3("Tux"))
    
