#!/usr/bin/env python3


def odd(seq=None):
    """Expects a sequence of integers and returns a list with
       only odd integers

    >>> odd()
    []
    >>> odd([])
    []
    >>> odd([1, 2])
    [1]
    >>> odd([1, 3, 5, 6, 8])
    [1, 3, 5]
    >>> odd([2, 4])
    []
    """
    if seq is None:
        return []
    result = []
    for item in seq:
        if item % 2:
            result.append(item)
    return result


def gen_odd(seq=None):
    """Expects a sequence of integers and returns a generator with
       only odd integers

    >>> list(gen_odd())
    []
    >>> list(gen_odd([]))
    []
    >>> list(gen_odd([1, 2]))
    [1]
    >>> list(gen_odd([1, 3, 5, 6, 8]))
    [1, 3, 5]
    >>> list(gen_odd([2, 4]))
    []
    """
    if seq is None:
        return []
    for item in seq:
        if item % 2:
            yield item


def even(seq=None):
    """Expects a sequence of integers and returns a list with
       only even integers

    >>> even()
    []
    >>> even([])
    []
    >>> even([1, 2])
    [2]
    >>> even([1, 3, 5, 6, 8])
    [6, 8]
    >>> even([2, 4])
    [2, 4]
    """
    result = []
    if seq is None:
        return []
    for item in seq:
        if not item % 2:
            result.append(item)
    return result


def gen_even(seq=None):
    """Expects a sequence of integers and returns a generator with
       only even integers

    >>> list(gen_even())
    []
    >>> list(gen_even([]))
    []
    >>> list(gen_even([1, 2]))
    [2]
    >>> list(gen_even([1, 3, 5, 6, 8]))
    [6, 8]
    >>> list(gen_even([2, 4]))
    [2, 4]
    """
    if seq is None:
        return []
    for item in seq:
        if not item % 2:
            yield item


if __name__ == "__main__":
    import doctest
    doctest.testmod()
