## Python-Projekte anlegen, mit Test-Cases u.a.

1. Einrichten der virtuellen Python-Umgebung:
   $ mkdir py; cd py
   $ pyvenv .env
   $ source .env/bin/activate
   $ pip install -U pip setuptools

2. Anlegen des Python-Projekts
   $ cookiecutter hg:ionelmc/cookiecutter-pylibrary
   => Fragen beantworten oder Standardauswahl akzeptieren

3. Einrichten von Git
   $ cd ~
   $ git clone http://github.com/magicmonty/bash-git-prompt.git ~/.bash-git-prompt
   $ vi ~/.alias
   alias gitprompt="source ~/.bash-git-prompt/gitprompt.sh"
   $ source ~/.alias
   $ gitprompt

4. Arbeiten und Testen
   $ chmod +x setup.py
   $ ./setup.py develop
   $ py.test -v tests