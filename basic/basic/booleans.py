#!/usr/bin/python
# -*- coding: UTF-8 -*-
#
# Für neue print()-Funktion
from __future__ import print_function

x=True
y=False

print("x=", x, " y=", y, sep="" )
print("x and y", x and y)
print("x or y", x or y)
print("not (x and y)", not (x and y))
