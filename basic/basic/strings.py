#!/usr/bin/python
# -*- coding: UTF-8 -*-
#
# Für neue print()-Funktion
from __future__ import print_function

URL=["http://docs.python.org/2.7/tutorial/introduction.html#strings",
     "http://docs.python.org/2.7/library/stdtypes.html#string-methods"]


# Drei Strings definieren:
s1="This is a string"
s2="This is another string"
s3="""Ein dreifach gequoteter String wie hier kann auch 'Quotes'
und "Quotes" enthalten. Wir können sogar Zeilenumbrüche \
maskieren, so ist dieser String nur zwei Zeilen lang."""


# Ausgeben
print("--- Strings ausgeben ---")
print("  s1", s1)
print("  s1 (mit sep)", s1, sep="=")
print("  s1", s1, ", s2", s2, sep="=")
print("  Mit Formatstring: s1='%s'" % s1)
print(u"  Oder durch Escape-Sequenzen als: \\N{euro sign}=\N{euro sign} \\u20ac=\u20ac")

print("--- Länge eines Strings ---")
print("  len(s1)", len(s1))
print("  len(s2)", len(s2))

#
print("\n--- Auf Stringwerte zugreifen  ---")
print("Syntax: seq[INDEX]")
print("  s1='%s'" % s1)
print("      0123456789|123456789")
print("  s1[5]='%s'" % s1[5])
print("  s1[3]='%s'" % s1[3])
print("  s1[-5]='%s'" % s1[-5])


print("\n--- Slicing ---")
print("Syntax: seq[START], seq[START:END], seq[START:END:STEP]")
print("  s1[:5]='%s'" % s1[:5])
print("  s1[5:]='%s'" % s1[5:])
print("  s1[2:5]='%s'" % s1[2:5])
print("  s1[::2]='%s'" % s1[::2])

print("\n--- Strings vergleichen ---")
print("  s1 == s2", s1==s2)
print("  s1[:6] == s2[:6]", s1[:6] == s2[:6])
print("  s1 == s3", s1==s3)

print("\n--- Strings auftrennen und zusammensetzen ---")
print("Syntax: seq.split(ZEICHEN)")
print("  s1.split(' ')", s1.split(' '))
print("  s1.split(' is ')", s1.split(' is '))
print("  s1 + s2='%s'", s1+s2)
print("  '--'.join([s1,s2])='%s'" % '--'.join([s1,s2]) )
print("  '--'.join(s1+s2)='%s'" % '--'.join(s1+s2) )


print("\n--- Strings umwandeln ---")
print("Syntax: seq.FUNC(...)")
print("  Großschreibung:  s1.upper()='%s'"  % s1.upper() )
print("  Kleinschreibung: s1.lower()='%s'"  % s1.lower() )
print("  Titelschreibwse: s1.title()='%s'"  % s1.title() )
print("  Vertauschen:     s1.swapcase()='%s'" % s1.swapcase() )

print("  => Siehe auch Python Dokumentation", "\n  ".join(URL) )
