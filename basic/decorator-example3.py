#!/usr/bin/python3
#
# More examples, see
# https://wiki.python.org/moin/PythonDecoratorLibrary
#

from functools import wraps
import time


def timing(func):

    """
    Outputs the time a function takes
    to execute.
    """
    @wraps(func)
    def wrapper():
        t1 = time.time()
        func()
        t2 = time.time()
        return "Time it took to run for the function {!r}: {}".format(func.__name__,
                                                                      t2 - t1)
    return wrapper


def deprecated(func):
    '''This is a decorator which can be used to mark functions
    as deprecated. It will result in a warning being emitted
    when the function is used.'''
    @wraps(func)
    def wrapper(*args, **kwargs):
        warnings.warn("Call to deprecated function {}.".format(func.__name__),
                  category=DeprecationWarning)
        return func(*args, **kwargs)
    return wrapper


def print_callcount(func):
    x = 0
    @wraps(func)
    def wrapper(*args, **kwargs):
        nonlocal x
        x += 1
        # print("%d calls so far" % x)
        return x, func(*args, **kwargs)

    return wrapper


@print_callcount
def lol(x):
    return "lol"*x


@timing
def my_function():
    num_list = []
    for num in (range(0, 10000)):
        num_list.append(num)
    time.sleep(1)
    return "Sum of all the numbers: {}".format(sum(num_list))


if __name__ == "__main__":
    print(my_function())
    print(lol(3))
    print(lol(2))
