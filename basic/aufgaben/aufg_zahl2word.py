#!/usr/bin/python2
# -*- coding: UTF-8 -*-

from __future__ import print_function

digits=['null', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine']

def zahl2word(zahl):
    a = zahl
    result=[]
    while a != 0:
      rest = a % 10
      a = a // 10
      result.insert(0, digits[rest])
    return result

if __name__ == "__main__":
   print( "12 =",  zahl2word(12) )
   print( "967 =", zahl2word(967) )

