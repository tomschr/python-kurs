#!/usr/bin/python2

import sys
import doctest

def main():
  runner = doctest.DebugRunner(verbose=True)
  dt = open(sys.argv[1], "r").readlines()
  dt = "".join(dt)
  test = doctest.DocTestParser().get_doctest(dt, {}, 'foo', 'foo.py', 0)
  # test=doctest.DocTestParser().get_examples(dt)
  try:
     runner.run(test)
  except UnexpectedException, failure:
     pass


if __name__ == "__main__":
  if len(sys.argv) == 2 and (sys.argv[1] not in ("-h", "--help")):
    main()
  else:
    print("{0} doctest".format(sys.argv[0]))
    sys.exit(1)

