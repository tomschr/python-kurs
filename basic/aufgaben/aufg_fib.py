#!/usr/bin/python
# -*- coding: UTF-8 -*-

"""
Returns the Fibonacci numbers

        ╭  0            if n = 0
  Fn =  │  1            if n = 1
        ╰  Fn-1 + Fn-2  if n > 1

"""

def fib_print(n):
   """Print a Fibonacci series up to n"""
   a, b = 0, 1
   while a < n:
     print a,
     a, b = b, a+b



def fib_list(n):
   """Return a list containing Fibonacci series up to n"""
   result=[]
   a, b = 0, 1
   while a < n:
     result.append(a)
     a, b = b, a+b
   return result



 
def fib_gen(n):
   """Return a generator containing Fibonacci series up to n"""
   a, b = 0, 1
   while a < n:
     yield a
     a, b = b, a+b




def fib_gen2():
   """Return a generator containing Fibonacci series"""
   a, b = 0, 1
   yield a
   yield b
   while True:
     a, b = b, a+b
     yield b

