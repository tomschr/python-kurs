#!/usr/bin/python
# -*- coding: UTF-8 -*-

from __future__ import print_function

URL=["http://docs.python.org/2.7/reference/lexical_analysis.html#strings"
    ]

s1="Dies ist ein String"
s2="Hallo Welt"
count=0

print("Zählweise fängt immer bei Null an.\n%s Aufgaben %s" % ("-"*5, "-"*5))

# -----------------------------------------------------------
count+=1
print("Aufgabe %i: Gib den String 's1' aus:" % count)
#

count+=1
print("Aufgabe %i: Gib das erste Zeichen von String 's1' aus:" % count)
#

count+=1
print("Aufgabe %i: Gib das letzte Zeichen von String 's1' aus:" % count)
#

count+=1
print("Aufgabe %i: Gib das Zeichen an Position 5 aus:" % count)
#

count+=1
print("Aufgabe %i: Gib alle Zeichen von String 's1' *ab* Position 4 aus:" % count)
#

count+=1
print("Aufgabe %i: Gib alle Zeichen von String 's1' *bis* zur Position 5 aus:" % count)
#

count+=1
print("Aufgabe %i: Gib jedes zweite Zeichen von String 's1' aus:" % count)
#

count+=1
print("Aufgabe %i: Erzeuge String 's1' insgesamt dreimal:" % count)
#

count+=1
print("Aufgabe %i: Gib die Länge des Strings 's1' aus:" % count)
#

count+=1
print("Aufgabe %i: Ist an Position 4 ein Leerzeichen?" % count)
#

count+=1
print("Aufgabe %i: Ist an Position 4 UND 8 ein Leerzeichen?" % count)
#

count+=1
print("Aufgabe %i: Sind die Strings 's1' und 's2' gleich?" % count)
#

count+=1
print("Aufgabe %i: Wandle 's1' in Großbuchstaben um:" % count)
#

count+=1
print("Aufgabe %i: Wandle 's1' in Kleinbuchstaben um:" % count)
#

count+=1
print("Aufgabe %i: Besteht der String 's1' nur aus Kleinbuchstaben?" % count)
#

count+=1
print("Aufgabe %i: Endet der String 's1' mit dem Wort 'string'?" % count)
#

count+=1
print("Aufgabe %i: Ersetze das Zeichen 'S' in String 's1' durch 'x':" % count)
#

count+=1
print("Aufgabe %i: Zerteile den String 's1' an Wortgrenzen bzw. Leerzeichen (ergibt eine Liste als Ergebnis):" % count)
#

count+=1
print("Aufgabe %i: Zentriere den String 's1' innerhalb von 80 Zeichen:" % count)
#

count+=1
print("Aufgabe %i: Wie fügt man das Euro-Zeichen (U+20AC) in einen String ein?" % count)
#


