#!/usr/bin/python2
# -*- coding: UTF-8 -*-

from __future__ import print_function, division
import sys

def mean(zahlen):
  """Erwartet eine Liste von Zahlen
     Rückgabe: Durchschnittswert
  """
  return sum(zahlen)/len(zahlen)

print("Übergabe:", sys.argv )

zahlen=[]
for i in sys.argv[2:]:
  try:
    zahlen.append(int(i))
  except:
    print("Überspringe {} da keine Zahl".format(i))

print("Maximumwert:", max(zahlen))
print("Minimumwert:", min(zahlen))
print("Durchschnitt:", mean(zahlen))

