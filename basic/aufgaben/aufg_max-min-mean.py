#!/usr/bin/python3

print("Type integers, each followed by Enter; or ^D or ^Z to finish")

result=[]

#while True:
#    try:
#        line = input()
#        if line:
#            result.append(int(line))
#    except ValueError as err:
#        print(err)
#        continue
#    except EOFError:
#        break

while True:
    line = input()
    if line.strip():
        result.append(int(line))
    else:
        break

if result:
    print("count =", len(result),
          "total =", sum(result),
          "mean =", sum(result) / len(result))
