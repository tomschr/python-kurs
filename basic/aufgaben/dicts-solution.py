#!/usr/bin/python
# -*- coding: UTF-8 -*-
#
# Für neue print()-Funktion
from __future__ import print_function

URL=["http://docs.python.org/2.7/tutorial/datastructures.html#dictionaries"
     "http://docs.python.org/2.7/library/stdtypes.html#sequence-types-str-unicode-list-tuple-bytearray-buffer-xrange",
    ]

count=0

print("Zählweise fängt immer bei Null an.\n%s Aufgaben %s" % ("-"*5, "-"*5))

D1={"a": 10, "b": 12, "c": 20}

# -----------------------------------------------------------
count+=1
print("Aufgabe %i: Gib das Dict 'D1' aus:" % count)
print("  D1=", D1)

count+=1
print("Aufgabe %i: Gib den Wert von Schlüssel 'a' aus:" % count)
print("  D1['a'] =", D1['a'])

count+=1
print("Aufgabe %i: Gib _alle_ Werte von Dict 'D1' aus:" % count)
print("  D1.values() =", D1.values())

count+=1
print("Aufgabe %i: Gib _alle_ Schlüssel (=Keys) von Dict 'D1' aus:" % count)
print("  D1.keys() =", D1.keys())

count+=1
print("Aufgabe %i: Hat das Dict 'D1' den Schlüssel 'c'?" % count)
print("  D1.has_key('c') = ", D1.has_key('c'))
print("  'c' in D1 = ", 'c' in D1)

count+=1
print("Aufgabe %i: Hat das Dict 'D1' den Wert 50?" % count)
print("  '50' in D1.values() = ", '50' in D1.values() )

count+=1
print("Aufgabe %i: Ergänze das Dict 'D1' durch das Paar x=50:" % count)
D1.update(x=50)
print("  D1.update(x=50)" )

count+=1
print("Aufgabe %i: Füge das Dict A={'d': 35, 'y': -5, 'a': 5} in das Dict 'D1' ein:" % count)
A={'d': 35, 'y': -5, 'a': 5}
print("  D1.update(A)")

count+=1
print("Aufgabe %i: Gibt den Wert von Dict 'D1' für die Schlüssel 'x' und 'z' zurück;\n"
      "%s wenn ein Schlüssel nicht existiert, soll 'N/a' zurückgegeben werden" % (count, " "*len("Aufgabe %i:" % count)))
print("  D1.get('x', 'N/a'), D1.get('z', 'N/a') = ", D1.get('x', 'N/a'), D1.get('z', 'N/a') )

# EOF
