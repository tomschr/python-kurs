#!/usr/bin/python
# -*- coding: UTF-8 -*-
#
# Für neue print()-Funktion
from __future__ import print_function

URL=["http://docs.python.org/2.7/tutorial/introduction.html#lists",
     "http://docs.python.org/2.7/library/stdtypes.html#sequence-types-str-unicode-list-tuple-bytearray-buffer-xrange",
    ]

count=0

print("Zählweise fängt immer bei Null an.\n%s Aufgaben %s" % ("-"*5, "-"*5))

L1=[3,10,7,4,-4,]

# -----------------------------------------------------------
count+=1
print("Aufgabe %i: Gib die Liste 'L1' aus:" % count)

count+=1
print("Aufgabe %i: Wieviele Elemente enthält die Liste 'L1'?" % count)

count+=1
print("Aufgabe %i: Füge der Liste 'L1' die Zahl 5 hinzu:" % count)

count+=1
print("Aufgabe %i: Füge am Anfang der Liste 'L1' die Zahl 11 hinzu:" % count)

count+=1
print("Aufgabe %i: Gib das erste Element der Liste 'L1' aus:" % count)

count+=1
print("Aufgabe %i: Gib das dritte Element der Liste 'L1' aus:" % count)

count+=1
print("Aufgabe %i: Gib das letzte Element der Liste 'L1' aus:" % count)

count+=1
print("Aufgabe %i: Gib das vorletzte Element der Liste 'L1' aus:" % count)

count+=1
print("Aufgabe %i: Lass die Liste 'L1' sortieren:" % count)

count+=1
print("Aufgabe %i: An welcher Stelle befindet sich die Zahl 5?" % count)

count+=1
print("Aufgabe %i: Gib die Summe von Liste 'L1' aus:" % count)

count+=1
print("Aufgabe %i: Gib den Maximum- und Minimumwert der Liste 'L1' aus:" % count)


