#!/usr/bin/python
# -*- coding: UTF-8 -*-
#
# Für neue print()-Funktion
from __future__ import print_function

URL=["http://docs.python.org/2.7/tutorial/datastructures.html#dictionaries"
     "http://docs.python.org/2.7/library/stdtypes.html#sequence-types-str-unicode-list-tuple-bytearray-buffer-xrange",
    ]

count=0

print("Zählweise fängt immer bei Null an.\n%s Aufgaben %s" % ("-"*5, "-"*5))

D1={"a": 10, "b": 12, "c": 20}

# -----------------------------------------------------------
count+=1
print("Aufgabe %i: Gib das Dict 'D1' aus:" % count)

count+=1
print("Aufgabe %i: Gib den Wert von Schlüssel 'a' aus:" % count)

count+=1
print("Aufgabe %i: Gib _alle_ Werte von Dict 'D1' aus:" % count)

count+=1
print("Aufgabe %i: Gib _alle_ Schlüssel (=Keys) von Dict 'D1' aus:" % count)

count+=1
print("Aufgabe %i: Hat das Dict 'D1' den Schlüssel 'c'?" % count)

count+=1
print("Aufgabe %i: Hat das Dict 'D1' den Wert 50?" % count)

count+=1
print("Aufgabe %i: Ergänze das Dict 'D1' durch das Paar x=50:" % count)

count+=1
print("Aufgabe %i: Verschmelze das Dict A={'d': 35, 'y': -5, 'a': 5} mit dem Dict 'D1':" % count)

count+=1
print("Aufgabe %i: Gibt den Wert von Dict 'D1' für die Schlüssel 'x' und 'z' zurück;\n"
      "%s wenn ein Schlüssel nicht existiert, soll 'N/a' zurückgegeben werden" % (count, " "*len("Aufgabe %i:" % count)))

# EOF
