#!/usr/bin/python
# -*- coding: UTF-8 -*-

from __future__ import print_function

URL=["http://docs.python.org/2.7/reference/lexical_analysis.html#strings"
    ]

s1="Dies ist ein String"
s2="Hallo Welt"
count=0

print("Zählweise fängt immer bei Null an.\n%s Aufgaben %s" % ("-"*5, "-"*5))

# -----------------------------------------------------------
count+=1
print("Aufgabe %i: Gib den String 's1' aus:" % count)
print("  s1 = ", repr(s1))

count+=1
print("Aufgabe %i: Gib das erste Zeichen von String 's1' aus:" % count)
x="s1[0]"
print("  %s =" % x, eval(x))

count+=1
print("Aufgabe %i: Gib das letzte Zeichen von String 's1' aus:" % count)
x="s1[-1]"
print("  %s =" % x, eval(x))

count+=1
print("Aufgabe %i: Gib das Zeichen an Position 5 aus:" % count)
x="s1[4]"
print("  %s =" % x, eval(x))

count+=1
print("Aufgabe %i: Gib alle Zeichen von String 's1' *ab* Position 4 aus:" % count)
x="s1[4:]"
print("  %s =" % x, eval(x))

count+=1
print("Aufgabe %i: Gib alle Zeichen von String 's1' *bis* zur Position 5 aus:" % count)
x="s1[:5]"
print("  %s =" % x, eval(x))

count+=1
print("Aufgabe %i: Gib jedes zweite Zeichen von String 's1' aus:" % count)
x="s1[::2]"
print("  %s =" % x, eval(x))

count+=1
print("Aufgabe %i: Erzeuge String 's1' insgesamt dreimal:" % count)
x="3*s1"
print("  %s =" % x, eval(x))

count+=1
print("Aufgabe %i: Gib die Länge des Strings 's1' aus:" % count)
x="len(s1)"
print("  %s =" % x, eval(x))

count+=1
print("Aufgabe %i: Ist an Position 4 ein Leerzeichen?" % count)
x="s1[4] == ' '"
print("  %s =" % x, eval(x))

count+=1
print("Aufgabe %i: Ist an Position 4 UND 8 ein Leerzeichen?" % count)
x="s1[4] == ' ' and s1[8] == ' '"
print("  %s =" % x, eval(x))
x="s1.index(' ', 4, 5) and s1.index(' ', 8, 9)"
print("  %s =" % x, eval(x))

count+=1
print("Aufgabe %i: Sind die Strings 's1' und 's2' gleich?" % count)
x="s1 == s2"
print("  %s =" % x, eval(x))

count+=1
print("Aufgabe %i: Wandle 's1' in Großbuchstaben um:" % count)
x="s1.upper()"
print("  %s =" % x, eval(x)) 

count+=1
print("Aufgabe %i: Wandle 's1' in Kleinbuchstaben um:" % count)
x="s1.lower()"
print("  %s =" % x, eval(x))

count+=1
print("Aufgabe %i: Besteht der String 's1' nur aus Kleinbuchstaben?" % count)
x="s1.islower()"
print("  %s =" % x, eval(x))

count+=1
print("Aufgabe %i: Endet der String 's1' mit dem Wort 'string'?" % count)
x="s1.endswith('string')"
print("  %s =" % x, eval(x))

count+=1
print("Aufgabe %i: Ersetze das Zeichen 'S' in String 's1' durch 'x':" % count)
x="s1.replace('S', 'x')"
print("  %s =" % x, eval(x))

count+=1
print("Aufgabe %i: Zerteile den String 's1' an Wortgrenzen bzw. Leerzeichen (ergibt eine Liste als Ergebnis):" % count)
x="s1.split(' ')"
print("  %s =" % x, eval(x))

count+=1
print("Aufgabe %i: Zentriere den String 's1' innerhalb von 80 Zeichen:" % count)
x="s1.center(80)"
print("  %s =" % x, repr(eval(x)))

count+=1
print("Aufgabe %i: Wie fügt man das Euro-Zeichen (U+20AC) in einen String ein?" % count)
s3='Euro-Zeichen \u20AC'
print("  s3='%s'" % s3)

