from unicodedata import category

def split(s):
    """Splitting a space separated text into a list

    :param str s: the string to split
    :return: a list of all the words

    >>> split("hello world")
    ['hello', 'world']
    >>> split("hello World")
    ['hello', 'World']
    >>> split("hello, World!")
    ['hello,', 'World!']
    >>> split("--Hello--  ##World?!")
    ['--Hello--', '##World?!']
    >>> split("01Hello,  _6Wörld?!")
    ['01Hello,', '_6Wörld?!']
    """                          
    result=[]
    part=[]
    s = s.strip()
    i, l = 0, len(s)
    while i < l:
        c = s[i]
        if category(c) not in ('Zs', 'Cc', 'Cn', 'Cf'):
            part.append(c)
        elif (i+1 < l) and s[i+1].isspace():
            pass
        else:
            result.append("".join(part))
            part = []
        i += 1

    result.append("".join(part))
    return result
