#!/usr/bin/python
# -*- coding: UTF-8 -*-
#
# Für neue print()-Funktion
from __future__ import print_function

URL=[
    ]

count=0



# -----------------------------------------------------------
count+=1
print("Aufgabe %i: Wandle den String '16' in eine Zahl um:" % count)

count+=1
print("Aufgabe %i: Wandle die Dezimalzahl 10 in eine Hexadezimalzahl um:" % count)

count+=1
print("Aufgabe %i: Wandle die Dezimalzahl 10 in eine Oktalzahl um:" % count)

count+=1
print("Aufgabe %i: Wandle die Zahl 12 in einen String um:" % count)

count+=1
print("Aufgabe %i: Wandle die Liste [3, 5] in einen String um:" % count)

count+=1
print("Aufgabe %i: Wandle die Liste [3, 5] in einen String um:" % count)

count+=1
print("Aufgabe %i: Wandle das Dict {'a':3, 'b':5} in einen String um:" % count)

# EOF
