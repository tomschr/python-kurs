#!/usr/bin/env python3

import functools
from itertools import accumulate
import operator
from math import log, exp
try:
    import numpy as np
except ImportError:
    np = None
import random
import timeit

seq = [random.choice(range(1,10)) for _ in range(1, 2000)]


def mul(iterable, start=1):
    """Return the multiplication of a 'start' value (default: 1)
       plus an iterable of numbers

       When the iterable is empty, return the start value
       This function is intended specifically for use with numeric
       values and may reject non-numeric types.

       >>> mul([])
       1
       >>> seq = [1, 2, 3]
       >>> mul(seq)
       6
    """
    result=start
    for item in iterable:
        result *= item
    return result


def mul_func(iterable, start=1):
    """Return the multiplication of a 'start' value (default: 1)
       plus an iterable of numbers

       When the iterable is empty, return the start value
       This function is intended specifically for use with numeric
       values and may reject non-numeric types.

       >>> mul_func([])
       1
       >>> seq = [1, 2, 3]
       >>> mul_func(seq)
       6
    """
    return functools.reduce(operator.mul, iterable, start)


def mul_exp(iterable, start=1):
    """Return the multiplication of a 'start' value (default: 1)
       plus an iterable of numbers

       When the iterable is empty, return the start value
       This function is intended specifically for use with numeric
       values and may reject non-numeric types.

       >>> mul_exp([])
       1.0
       >>> seq = [1, 2, 3]
       >>> mul_exp(seq)
       6.0
    """
    return start * exp(sum(map(log, iterable)))


def mul_lambda(iterable, start=1):
    """Return the multiplication of a 'start' value (default: 1)
       plus an iterable of numbers

       When the iterable is empty, return the start value
       This function is intended specifically for use with numeric
       values and may reject non-numeric types.

       >>> mul_lambda([])
       1
       >>> seq = [1, 2, 3]
       >>> mul_lambda(seq)
       6
    """
    return functools.reduce(lambda x, y: x*y, iterable, start)


def mul_np(iterable, start=1):
    """Return the multiplication of a 'start' value (default: 1)
       plus an iterable of numbers

       When the iterable is empty, return the start value
       This function is intended specifically for use with numeric
       values and may reject non-numeric types.

       >>> mul_np([])
       1
       >>> seq = [1, 2, 3]
       >>> mul_np(seq)
       6
    """
    return np.prod(iterable, dtype=int, initial=start)


def mul_accum(iterable, start=1):
    """Return the multiplication of a 'start' value (default: 1)
       plus an iterable of numbers

       When the iterable is empty, return the start value
       This function is intended specifically for use with numeric
       values and may reject non-numeric types.

       >>> mul_accum([])
       1
       >>> seq = [1, 2, 3]
       >>> mul_accum(seq)
       6
    """
    value = start
    for value in accumulate(iterable, operator.mul):
        pass
    return value


def mul_while(iterable, start=1):
    """Return the multiplication of a 'start' value (default: 1)
       plus an iterable of numbers

       When the iterable is empty, return the start value
       This function is intended specifically for use with numeric
       values and may reject non-numeric types.

       >>> mul_while([])
       1
       >>> seq = [1, 2, 3]
       >>> mul_while(seq)
       6
    """
    product = start
    i = 0
    while True:
        if i == len(iterable):
            break
        product *= iterable[i]
        i += 1
    return product


def mul_while2(iterable, start=1):
    """Return the multiplication of a 'start' value (default: 1)
       plus an iterable of numbers

       When the iterable is empty, return the start value
       This function is intended specifically for use with numeric
       values and may reject non-numeric types.

       >>> mul_while([])
       1
       >>> seq = [1, 2, 3]
       >>> mul_while(seq)
       6
    """
    product = start
    i = 0
    while i < len(iterable):
        product *= iterable[i]
        i += 1
    return product


def mul_next(iterable, start=1):
    """Return the multiplication of a 'start' value (default: 1)
       plus an iterable of numbers

       When the iterable is empty, return the start value
       This function is intended specifically for use with numeric
       values and may reject non-numeric types.

       >>> mul_next([])
       1
       >>> seq = [1, 2, 3]
       >>> mul_next(seq)
       6
    """
    it = iter(iterable)
    product = start
    try:
        while True:
            product *= next(it)
    except StopIteration:
        return product

def test():
    funcs = ['mul(seq)',
             'mul_func(seq)',
             'mul_lambda(seq)',
             'mul_accum(seq)',
             'mul_while(seq)',
             'mul_while2(seq)',
             'mul_next(seq)',
             # 'mul_exp(seq)', # fails with big numbers
             ]
    # Only include mul_np, when numpy is available:
    if np is not None:
        funcs.append('mul_np(seq)')

    for func in funcs:
        res = timeit.timeit(func, number=10000, globals=globals())
        print("Function %s needs %s" % (func, res))

    print("--- done ---")



if __name__ == "__main__":
    import doctest
    doctest.testmod()
    test()
