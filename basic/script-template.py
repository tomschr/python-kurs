#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
Globaler Docstring
"""

# Version String dreistellig, empfohlen semver.org
__version__="1.0.0"

# Dein Name + E-Mail-Adresse
__author__="Tux Penguin <tux@island.com>"

# System imports
# User imports

# Globale Funktionen

# Für Bibliotheken
if __name__ == "__main__":
    # Hier die Hauptfunktionalität aufrufen
