#!/usr/bin/env python

from os import path
# Always prefer setuptools over distutils
from setuptools import setup, find_packages


setupdict = dict(
   name='tuxfish',
   version='0.1.0',
   description='Makes a lot of fish',
   url='https://github.com/tux/tuxfish',
   # Author details
   author='Tux Penguin',
   author_email='tux (AT) example.org',
   license='GPL-3.0',
   # See https://pypi.python.org/pypi?%3Aaction=list_classifiers
   classifiers=[
      'Development Status :: 5 - Production/Stable'
      #
      'Topic :: Documentation',
      'Topic :: Software Development :: Documentation',
      'Intended Audience :: Developers',
      # The license:
      'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
      # Supported Python versions:
      'Programming Language :: Python :: 3.3',
      'Programming Language :: Python :: 3.4',
      'Programming Language :: Python :: 3.5',
   ],
   keywords='example, Tux, fish',
   include_package_data = True,
   # You can just specify the packages manually here if your project is
   # simple. Or you can use find_packages().
   packages=find_packages('src'),
   package_dir={'': 'src'},
   # install_requires=[''], #

   # If there are data files included in your packages that need to be
   # installed, specify them here.  If using Python 2.6 or less, then these
   # have to be included in MANIFEST.in as well.
   #package_data={
   #     '': ['src/tuxfish/*.xsl'],
   #},
   extras_require={
        'test': ['pytest', 'coverage'],
   },
   entry_points={
        'console_scripts': [
            'tuxfish=tuxfish:main',
        ],
    },
)

setup(**setupdict)
