import pytest

def test_version():
    """Check if version is available and set"""
    from tuxfish.version import __version__
    assert __version__


def test_version_from_cli(capsys):
    """Checks if option --version creates a correct version"""
    from tuxfish import main

    with pytest.raises(SystemExit):
        main(["--version"])

