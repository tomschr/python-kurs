#!/usr/bin/python3
# -*- coding: UTF-8 -*-
#
# Run the tests with:
# $ python3 -m doctest -o IGNORE_EXCEPTION_DETAIL -v versionhistory.py

"""
Test cases:
>>> from version import Version
>>> vh1 = VersionHistory(1, 2, 1)
>>> str(vh1)
'1.2.1'
>>> repr(vh1)
'VersionHistory(1, 2, 1)'
>>> vh1.bump_major()
VersionHistory(2, 0, 0)
>>> vh1.bump_major().bump_minor()
VersionHistory(3, 1, 0)
>>> vh1 + Version(0, 1, 1)
VersionHistory(3, 2, 1)
>>> Version(0, 1, 1) + vh1
VersionHistory(3, 2, 1)
>>> vh1.history
[(1, 2, 1), (2, 0, 0), (3, 0, 0), (3, 1, 0)]
>>> vh1.clear()
>>> vh1.history
[]
"""

from version import Version


class VersionHistory(Version):
    """Class which preserves the history of versions"""

    def __init__(self, major, minor, release):
        """Stores and compares version strings and preserves history

        Versions contains major, minor, and release numbers as integers.

        Versions can be constructed by using:

        - with no arguments like Version(); this is equivalent like
          Version(0,0,0)
        - with positional arguments like Version(release=4); this is equivalent
          like Version(0,0,4)
        - with another Version like Version(Version(1, 2, 3)); this is
          equivalent like Version(1, 2, 3)
        """
        super().__init__(major, minor, release)
        self._history = [self.ver]

    def clear(self):
        """Clears the history"""
        self._history = []

    def bump_major(self):
        """Increase major version, set minor & release to zero.
           Adds new version to the history.
        """
        newver = super().bump_major()
        self._history.append(self.ver)
        return newver

    def bump_minor(self):
        """Increase minor version, set release to zero.
           Adds new version to the history.
        """
        newver = super().bump_minor()
        self._history.append(self.ver)
        return newver

    def bump_release(self):
        """Increase release version, leave major & minor untouched.
           Adds new version to the history.
        """
        newver = super().bump_release()
        self._history.append(self.ver)
        return newver

    @property
    def history(self):
        """History property"""
        return self._history

    def __iadd__(self, other):
        """self += other"""
        super().__iadd__(other)
        self._history.append(self.ver)
        return self

    def __add__(self, other):
        """self + other
           does NOT preserve history"""
        return super().__add__(other)

    def __radd__(self, other):
        """other + self
           equivalent as self + other; does NOT preserve history"""
        return super().__add__(other)


def _test():
    import doctest
    doctest.testmod(optionflags=doctest.IGNORE_EXCEPTION_DETAIL
                    | doctest.REPORT_UDIFF
                    | doctest.NORMALIZE_WHITESPACE
                    )


if __name__ == "__main__":
    _test()

    v1 = Version(1, 3)
    vh1 = VersionHistory(1, 2, 1)
    vh2 = VersionHistory(2, 12, 13)
    print(repr(vh1))
    # print(dir(vh1))
    print(vh1.history)
    vh1.bump_major()
    print(vh1.history)
    vh1.bump_minor()
    print(vh1.history)

    vh3 = vh1 + v1
    print("Adding {} + {} -> {}".format(vh1, v1, vh3))
    print(vh3.history)
    t1 = (1, 3, 5)
    print("Adding {} + {} -> {}".format(vh1, t1, vh1 + t1))

    vhr3 = v1 + vh1
    print(vhr3)
    print(vhr3.history)

# EOF
