#!/usr/bin/python3
#
# Run the tests with:
# $ python3 -m doctest -v -o IGNORE_EXCEPTION_DETAIL -o REPORT_UDIFF -o NORMALIZE_WHITESPACE version_v0.py


"""Eine Klasse für Versionsstrings

Ziel:
  - Alle Tests müssen fehlerfrei durchlaufen (s.u.) :-)
  - Jede Methode muss dokumentiert sein
  - Fehlermeldungen sollen aussagefähig sein

Aufgaben:
* Implementierung des "Konstruktors":
  - Über die Magic Method "__init__"
  - Erwartet 3 Argumente: major, minor und release
  - Alle Argumente sind Integer
  - Alle Argumente müssen positiv sein
  - Werden Argumente ausgelassen, werden sie mit Null initialisiert

Tipps:
- Implementiere major, minor und relase als Properties
- Verwende "Magic Methods" für die Vergleiche


Testcases (which will fail):

>>> Version()
Version(0, 0, 0)

>>> Version(-1)
Traceback (most recent call last):
ValueError: ...
>>> Version(minor=-1)
Traceback (most recent call last):
ValueError: ...
>>> Version(release=-1)
Traceback (most recent call last):
ValueError: ...

>>> str(Version(1,1,1))
'1.1.1'
>>> repr(Version(1,1,1))
'Version(1, 1, 1)'
"""

class Version:
    """Stores and compares version strings"""
    def __init__(self, ):  # Unvollständig
        raise NotImplementedError


if __name__=="__main__":
    import doctest
    doctest.testmod(optionflags=doctest.IGNORE_EXCEPTION_DETAIL
                    | doctest.REPORT_UDIFF
                    | doctest.NORMALIZE_WHITESPACE
                    )
