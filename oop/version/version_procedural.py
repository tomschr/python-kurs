#!/usr/bin/python3
"""
Beispiel eines prozeduralen "Versions-Datentypen"

Beschreibung:
Version ist ein Datentyp zum Verwalten von Versionsinformationen.
Die Information enthält einen Major, Minor und Patch Teil. Alle diese
Teile lassen sich erhöhen um die nächst höhere Version zu erhalten.
Eine Version kann formatiert werden

Version ist hier ein Tuple und besteht aus diesen Teilen:

 * Major (int)
 * Minor (int)
 * Patch (int)

"""


def checkversion(version):
    """Checks each part

    :param version: the version with major, minor, and patch
    :type version: tuple | list
    :raise: TypeError, if version is not a tuple or list
    :return: if all parts are integer (=True) or not (=False)
    :rtype: bool
    """
    validtypes = (tuple, list)
    if not isinstance(version, validtypes):
        raise TypeError("Expected %s, but got %s" %
                        ", ".join(validtypes),
                        type(version)
                        )
    if len(version) != 3:
        raise ValueError("Version does not contain enough parts")
    return all([isinstance(item, int) for item in version])


def format_version(version):
    """Format a version

    :param version: the version with major, minor, and patch
    :type version: tuple | list
    :return: a formatted version
    :rtype: str
    """
    if not checkversion(version):
        raise ValueError("Version contains parts which are not integer")
    return ".".join([str(v) for v in version])


def bump_major(version):
    """Bump the major part, all others are zero

    :param version: the version with major, minor, and patch
    :type version: tuple | list
    :return: the bumped version
    """
    if not checkversion(version):
        raise ValueError("Version contains parts which are not integer")
    return (version[0]+1, 0, 0)


def bump_minor(version):
    """Bump the minor part, don't change major, but set patch to zero

    :param version: the version with major, minor, and patch
    :type version: tuple | list
    :return: the bumped version
    """
    if not checkversion(version):
        raise ValueError("Version contains parts which are not integer")
    return (version[0], version[1]+1, 0)


def bump_patch(version):
    """Bump the patch part

    :param version: the version with major, minor, and patch
    :type version: tuple | list
    :return: the bumped version
    """
    if not checkversion(version):
        raise ValueError("Version contains parts which are not integer")
    return (version[0], version[1], version[2]+1)


def cmp_version(version1, version2):
    """Compares two versions

    :param version1: the first version with major, minor, and patch
    :type version1: tuple | list
    :param version2: the second version with major, minor, and patch
    :type version2: tuple | list
    """
    if not checkversion(version1):
        raise ValueError("Version1 contains parts which are not integer")
    if not checkversion(version2):
        raise ValueError("Version2 contains parts which are not integer")
    return (version1 > version2) - (version1 < version2)


if __name__ == "__main__":
    v1 = (1, 2, 3)
    v2 = (1, 2, 4)
    print("v1:", v1)
    print("v2:", v2)
    print("Comparing v1 against v2", cmp_version(v1, v2))
    print("Bump v1", bump_minor(v1))
