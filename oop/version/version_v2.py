#!/usr/bin/python3
# -*- coding: UTF-8 -*-
#
# Run the tests with:
# $ python3 -m doctest -v -o IGNORE_EXCEPTION_DETAIL -o REPORT_UDIFF -o NORMALIZE_WHITESPACE version_v2.py

"""

Test cases (all testcases works):

1. Initialization
>>> Version()
Version(0, 0, 0)
>>> Version(-1)
Traceback (most recent call last):
ValueError: major must be >0
>>> Version(minor=-1)
Traceback (most recent call last):
...
ValueError: minor must be >0
>>> Version(release=-1)
Traceback (most recent call last):
...
ValueError: release must be >0

2. Comparison
>>> Version(1,3) == Version(1,3,0)
True
>>> Version(1,2) < Version(1,2,1)
True
>>> Version(1,1,9) < Version(1,2)
True
>>> Version(1,1,1) > Version(1,0,1)
True
>>> Version(1,1,1) >= Version(1,0,1)
True
>>> Version(1,1,1) >= Version(1,1,1)
True
>>> Version(1,1,1) != Version(1,0,1)
True

3. Representation
>>> str(Version(1,1,1))
'1.1.1'
>>> repr(Version(1,1,1))
'Version(1, 1, 1)'
>>> eval(repr(Version(1,1,1))) == Version(1,1,1)
True
>>> tuple(Version(1,1,1))
(1, 1, 1)
>>> Version(1,2,3).ver
(1, 2, 3)

4. Calculation
>>> v1 = Version(1,1,1)
>>> v1 += Version(minor=2)
>>> v1
Version(1, 3, 1)
>>> v1 += Version(release=2)
>>> v1
Version(1, 3, 3)
>>> v1 += Version(major=1)
>>> v1
Version(2, 3, 3)

5. Conversion
>>> str(Version.fromhex('0x01020a'))
'1.2.10'
"""
from functools import total_ordering


@total_ordering
class Version:
    """Stores and compares version strings

    """
    def __init__(self, major=0, minor=0, release=0):
        """Stores and compares version strings

        Versions contains major, minor, and release numbers as integers.

        Versions can be constructed by using:

        - with no arguments like Version(); this is equivalent like
          Version(0,0,0)
        - with positional arguments like Version(release=4); this is equivalent
          like Version(0,0,4)
        """

        for name, value in [('major', major), ('minor', minor), ('release', release)]:
            self.__checkTypeAndValue(name, value)

        # Alternative implementation:
        # Use locals(); it returns a dictionary of all "local" variables
        # (including 'self').
        # We need to remove 'self' exlicity, as we are only interested in our
        # arguments. Otherwise we get errors in our checks:
        #
        #   d = locals()
        #   del d['self']
        #
        # One minor problem: order of dict does not match with order of
        # arguments.

        self._version = [major, minor, release]

    def __checkClass(self, other) -> bool:
        """Checks for the correct class"""
        if not isinstance(other, self.__class__):
            raise TypeError("Expected {} type, got {}".format(type(self), type(other)))
        return True

    def __checkType(self, name, value) -> bool:
        """Checks for allowed types (integers)"""
        if not isinstance(value, int):
            raise TypeError("Expected int type. But got {} for {}".format(type(value), name))
        return True

    def __checkGreaterNull(self, name, value) -> bool:# doctest: +SKIP
        """Checks if value < 0"""
        if value < 0:
            raise ValueError("{} must be >0".format(name))
        return True

    def __checkTypeAndValue(self, name, value):
        """Checks for allowed types and value <0"""
        self.__checkType(name, value)
        self.__checkGreaterNull(name, value)

    @property
    def major(self) -> int:
        """major property"""
        return self._version[0]
    @major.setter
    def major(self, value):
        self.__checkTypeAndValue("major", value)
        self._version[0] = value

    @property
    def minor(self) -> int:
        """minor property"""
        return self._version[1]
    @minor.setter
    def minor(self, value):
        self.__checkTypeAndValue("minor", value)
        self._version[1] = value

    @property
    def release(self) -> int:
        """release property"""
        return self._version[2]
    @release.setter
    def release(self, value):
        self.__checkTypeAndValue("release", value)
        self._version[2] = value

    @property
    def ver(self) -> list:
        """Property: ver -> [major, minor, release]"""
        return tuple(self._version)

    def __repr__(self) -> str:
        """repr(x)"""
        return "{}({}, {}, {})".format(self.__class__.__name__,
                                       *self.ver)

    def __iter__(self):
        """iter(x)"""
        return (v for v in self.ver)

    def __str__(self):
        """str(self)"""
        return "{}.{}.{}".format(*self.ver)

    def __lt__(self, other) -> bool:
        """self < other"""
        if not self.__checkClass(other):
            return NotImplemented
        return self.ver < other.ver

    def __eq__(self, other) -> bool:
        """self == other"""
        if not self.__checkClass(other):
            return NotImplemented
        return self.ver == other.ver

    def __iadd__(self, other) -> bool:
        """self += other"""
        self.__checkClass(other)

        self.major += other.major
        self.minor += other.minor
        self.release += other.release
        return self

    @classmethod
    def fromstring(cls, version):
        """Converts a string of 'a.b.c' into a Version object
        """
        if not isinstance(version, str):
            raise TypeError("Expected string for version, but got %s" % type(version))
        ver = version.split(".")
        ver = [int(v) for v in ver]
        return cls(*ver)

    @classmethod
    def fromhex(cls, other):
        """Converts a hex number `0xAABBCC` into a valid
           Version object, whereas AA is the major part,
           BB the minor part and CC the patch part
        """
        try:
            int(other, 16)
        except ValueError:
            raise ValueError("The value %s is not a hex number" % other)
        version = []
        # Remove the '0x' part:
        other = other[2:]
        for i in range(0,6,2):
            version.append(int('0x%s' % other[slice(i, i+2)], 16))
        return cls(*version)


if __name__=="__main__":
    import doctest
    doctest.testmod(optionflags=doctest.IGNORE_EXCEPTION_DETAIL
                                | doctest.REPORT_UDIFF
                                | doctest.NORMALIZE_WHITESPACE
                                )

# EOF
