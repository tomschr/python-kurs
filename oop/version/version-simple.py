#!/usr/bin/python3
# -*- coding: UTF-8 -*-
#
# Run the tests with:
# $ python3 -m doctest -o IGNORE_EXCEPTION_DETAIL -o REPORT_UDIFF -o ORMALIZE_WHITESPACE oop/version/version-namedtuple.py

"""
Test cases:

# Initialisations

>>> Version()
Version(0, 0, 0)
>>> Version(-1)
Traceback (most recent call last):
...
ValueError: Expected value > 0 for major, got -1
>>> Version(minor=-1)
Traceback (most recent call last):
ValueError: Expected value > 0 for minor, got -1
>>> Version(release=-1)
Traceback (most recent call last):
ValueError: Expected value > 0 for release, got -1


# Comparisons

>>> Version(1,3) == Version(1,3,0)
True
>>> Version(1,2) < Version(1,2,1)
True
>>> Version(1,1,9) < Version(1,2)
True
>>> Version(1,1,1) > Version(1,0,1)
True
>>> Version(1,1,1) >= Version(1,0,1)
True
>>> Version(1,1,1) >= Version(1,1,1)
True
>>> Version(1,1,1) != Version(1,0,1)
True
>>> Version(1,1,1) != (1,0,1)
True
>>> Version(1,1,1) != [1,0,1]
True


# Formatting

>>> str(Version(1,1,1))
'1.1.1'
>>> repr(Version(1,1,1))
'Version(1, 1, 1)'
>>> eval(repr(Version(1,1,1))) == Version(1,1,1)
True
>>> tuple(Version(1,1,1))
(1, 1, 1)


# Calculation

>>> v1 = Version(1,1,1)
>>> v1 += Version(minor=2)
>>> v1
Version(1, 3, 1)
>>> v1 += Version(release=2)
>>> v1
Version(1, 3, 3)
>>> v1 += Version(major=1)
>>> v1
Version(2, 3, 3)
>>> v1.minor += 1
>>> v1
Version(2, 4, 3)

# Special cases

>>> Version(1,3).ver
(1, 3, 0)
>>> Version(Version(1, 2, 3))
Version(1, 2, 3)
"""


class Version:
    """Stores and compares version strings"""
    def __init__(self, major=0, minor=0, release=0):
        self.major = major
        self.minor = minor
        self.release = release

    def __repr__(self):
        """repr(x) -> str"""
        return "{}({})".format(type(self).__name__,
                               str(self).replace(".", ", ")
                               )

    def __str__(self):
        """str(self) -> str"""
        return "{}.{}.{}".format(*self)

    def __iter__(self):
        """iter(x)"""
        yield self.major
        yield self.minor
        yield self.release
        # yield from (self.major, self.minor, self.release)


def _test():
    import doctest
    doctest.testmod(optionflags=doctest.IGNORE_EXCEPTION_DETAIL
                    | doctest.REPORT_UDIFF
                    | doctest.NORMALIZE_WHITESPACE
                    )

if __name__ == "__main__":
    _test()
