#!/usr/bin/python3
#
# Run the tests with:
# $ python3 -m doctest -v version.py


class Version:
    """Stores and compares version strings

    >>> Version()
    Version(0, 0, 0)
    >>> Version(-1)
    Traceback (most recent call last):
    ValueError: major must be >0
    >>> Version(minor=-1)
    Traceback (most recent call last):
    ValueError: minor must be >0
    >>> Version(release=-1)
    Traceback (most recent call last):
    ValueError: release must be >0
    >>> Version(1,3) == Version(1,3,0)
    True
    >>> Version(1,2) < Version(1,2,1)
    True
    >>> Version(1,1,9) < Version(1,2)
    True
    >>> Version(1,1,1) > Version(1,0,1)
    True
    >>> Version(1,1,1) >= Version(1,0,1)
    True
    >>> Version(1,1,1) >= Version(1,1,1)
    True
    >>> Version(1,1,1) != Version(1,0,1)
    True
    >>> str(Version(1,1,1))
    '1.1.1'
    >>> repr(Version(1,1,1))
    'Version(1, 1, 1)'

    """
    def __init__(self, major=0, minor=0, release=0):
        d = {'major': major, 'minor': minor, 'release': release}

        for key in d:
            value =d[key]
            if not isinstance(value, int):
                raise TypeError("Expected int type. But got {} for {}".format(
                type(value), value) )
            if value < 0:
                raise ValueError("{} must be >0".format(key))

        self._version = [ major, minor, release ]

    @property
    def major(self):
        return self._version[0]

    @property
    def minor(self):
        return self._version[1]

    @property
    def release(self):
        return self._version[2]

    @property
    def ver(self):
        return self._version

    def __repr__(self):
        """repr(self)"""
        return "{}({}, {}, {})".format(self.__class__.__name__,
                                       *self.ver)

    def __str__(self):
        """str(self)"""
        return "{}.{}.{}".format(*self.ver)

    def __lt__(self, other):
        """self < other"""
        return self.ver < other.ver

    def __le__(self, other):
        """self <= other"""
        return self.ver <= other.ver

    def __gt__(self, other):
        """self > other"""
        return not self.ver < other.ver

    def __eq__(self, other):
        """self == other"""
        return self.ver == other.ver

    def __ne__(self, other):
        """self != other"""
        return not self.ver == other.ver


def _test():
    import doctest
    doctest.testmod(optionflags=doctest.IGNORE_EXCEPTION_DETAIL
                    | doctest.REPORT_UDIFF
                    | doctest.NORMALIZE_WHITESPACE
                    )

if __name__ == "__main__":
    _test()

    # v1 = Version(1, 3)
    # v2 = Version(1, 2, 5)
    # v3 = Version(1, 3, 1)

    # print("Version v1=", v1, repr(v1))
    # print("v1 < v2", v1 < v2)
    # print("v1 < v3", v1 < v3)
