#!/usr/bin/python3
# -*- coding: UTF-8 -*-
#
# Run the tests with:
# $ python3 -m doctest -o IGNORE_EXCEPTION_DETAIL -v version.py

"""
Test cases:

# Initialisations

>>> Version()
Version(0, 0, 0)
>>> Version(-1)
Traceback (most recent call last):
ValueError: Expected value > 0 for major, got -1
>>> Version(minor=-1)
Traceback (most recent call last):
ValueError: Expected value > 0 for minor, got -1
>>> Version(release=-1)
Traceback (most recent call last):
ValueError: Expected value > 0 for release, got -1
>>> Version.fromsequence((1, 3, 4))
Version(1, 3, 4)
>>> Version.fromstring("1.6.4")
Version(1, 6, 4)

# Comparisons

>>> Version(1,3) == Version(1,3,0)
True
>>> Version(1,2) < Version(1,2,1)
True
>>> Version(1,1,9) < Version(1,2)
True
>>> Version(1,1,1) > Version(1,0,1)
True
>>> Version(1,1,1) >= Version(1,0,1)
True
>>> Version(1,1,1) >= Version(1,1,1)
True
>>> Version(1,1,1) != Version(1,0,1)
True
>>> Version(1,1,1) != (1,0,1)
True
>>> Version(1,1,1) != [1,0,1]
True


# Formatting

>>> str(Version(1,1,1))
'1.1.1'
>>> repr(Version(1,1,1))
'Version(1, 1, 1)'
>>> eval(repr(Version(1,1,1))) == Version(1,1,1)
True
>>> tuple(Version(1,1,1))
(1, 1, 1)


# Calculation

>>> v1 = Version(1,1,1)
>>> v1 += Version(minor=2)
>>> v1
Version(1, 3, 1)
>>> v1 += Version(release=2)
>>> v1
Version(1, 3, 3)
>>> v1 += Version(major=1)
>>> v1
Version(2, 3, 3)
>>> v1.minor += 1
>>> v1
Version(2, 4, 3)
>>> v1 + Version(minor=1, release=1)
Version(2, 5, 4)
>>> v1.bump_major()
Version(3, 0, 0)
>>> v1.bump_minor()
Version(3, 1, 0)
>>> v1.bump_release()
Version(3, 1, 1)
>>> v1.bump_major().bump_release().bump_minor()
Version(4, 1, 0)


# Special cases

>>> Version(1,3).ver
(1, 3, 0)
>>> Version(Version(1, 2, 3))
Version(1, 2, 3)
>>> a, b, c = Version(1, 2, 3)
>>> a, b, c
(1, 2, 3)
"""

from array import array
import re


class Version:
    """Stores and compares version strings"""

    _REGEX=re.compile(r"(?P<major>\d+)"
                      r"(?:\.(?P<minor>\d+))?"
                      r"(?:\.(?P<release>\d+))?"
                      )
    typecode = 'B'  # unsigned integer (1 byte)
    # ver_regex = re.compile(r"(?P<major>\d+)"
    #                        r"(?:\.(?P<minor>\d+))?"
    #                        r"(?:\.(?P<patch>\d+))?")

    def __init__(self, major=0, minor=0, release=0):
        """Stores and compares version strings

        Versions contains major, minor, and release numbers as integers.

        Versions can be constructed by using:

        - with no arguments like Version(); this is equivalent like
          Version(0,0,0)
        - with positional arguments like Version(release=4); this is equivalent
          like Version(0,0,4)
        - with another Version like Version(Version(1, 2, 3)); this is
          equivalent like Version(1, 2, 3)
        """
        self._version = [0, 0, 0]  # pre-occupy with some values
        if isinstance(major, self.__class__):
            other = major
            major, minor, release = other
            del other

        self.major = major
        self.minor = minor
        self.release = release

    @classmethod
    def _check(cls, **d):
        """Checks major, minor, and release for integer type and >0"""
        cls._check_length(d)
        for key in d:
            value = d[key]
            cls._checkType(key, value)
            cls._checkGreaterNull(key, value)

    @classmethod
    def _check_length(cls, other):
        """Check length of components """
        if len(other) > 3:
            raise OverflowError("A version can only have three parts: "
                                "major, minor, and patch. I've found {} "
                                "parts".format(len(d)))

    @classmethod
    def _check_class(cls, other):
        """Checks for the correct class"""
        # HINT: In this case, we don't pass "cls" but use Version instead.
        # Any derived class of Version returns true from isinstance()
        if not isinstance(other, (Version, tuple, list)):
            raise TypeError("Invalid type got {}".format(type(other)))

    @classmethod
    def _checkType(cls, name, value):
        """Checks for allowed types (integers)"""
        if not isinstance(value, int):
            raise TypeError("Got {} type for {}, "
                            "but expected Version".format(type(value), name))

    @classmethod
    def _checkGreaterNull(cls, name, value):  # doctest: +SKIP
        """Checks if value < 0"""
        if value < 0:
            raise ValueError("Expected value > 0 for {}, "
                             "got {}".format(name, value))

    @classmethod
    def _checkTypeAndValue(cls, name, value):
        """Checks for allowed types and value <0"""
        cls._checkType(name, value)
        cls._checkGreaterNull(name, value)

    @classmethod
    def fromsequence(cls, other):
        """Convert a sequence of 3 elements into a Version type"""
        if isinstance(other, cls):
            return other
        cls._check_class(other)
        cls._check_length(other)
        return cls(*other)

    @classmethod
    def fromstring(cls, other):
        """Convert a string as 'major[.minor[.patch]]' into a Version type"""
        match = cls._REGEX.search(other)
        if match is None:
            raise ValueError("Couldn't convert {} into a "
                             "Version type".format(other))
        #
        # version = [0 if v is None else int(v)
        #            for v in match.groups()]
        # return cls(*version)

        # Does only work, if the regex contains the same names as
        # the initialiser:
        version = {k: 0 if v is None else int(v)
                   for k, v in match.groupdict().items()}
        return cls(**version)

    @classmethod
    def frombytes(cls, octets):
        """Convert a byte sequence into a Version type"""
        typecode = chr(octets[0])
        memv = memoryview(octets[1:]).cast(typecode)
        return cls(*memv)

    @property
    def major(self):
        """major property"""
        return self._version[0]

    @major.setter
    def major(self, value):
        self._checkTypeAndValue("major", value)
        self._version[0] = value

    @property
    def minor(self):
        """minor property"""
        return self._version[1]

    @minor.setter
    def minor(self, value):
        self._checkTypeAndValue("minor", value)
        self._version[1] = value

    @property
    def release(self):
        """release property"""
        return self._version[2]

    @release.setter
    def release(self, value):
        self._checkTypeAndValue("release", value)
        self._version[2] = value

    @property
    def ver(self):
        """Property: ver -> (major, minor, release)"""
        return tuple(self._version)

    def bump_major(self):
        """Increase major version, set minor & release to zero"""
        self.major += 1
        self.minor, self.release = 0, 0
        return self

    def bump_minor(self):
        """Increase minor version, set release to zero"""
        self.minor += 1
        self.release = 0
        return self

    def bump_release(self):
        """Increase release version, leave major & minor untouched"""
        self.release += 1
        return self

    def __len__(self):
        """len(self) -> int"""
        return len(self._version)

    def __bytes__(self):
        """bytes(self)"""
        return (bytes([ord(self.typecode)]) +
                bytes(array(self.typecode, self)))

    def __repr__(self):
        """repr(x) -> str"""
        return "{}({}, {}, {})".format(type(self).__name__,
                                       *self.ver)

    def __iter__(self):
        """iter(x)"""
        return (v for v in self.ver)

    def __str__(self):
        """str(self) -> str"""
        return "{}.{}.{}".format(*self.ver)

    def __format__(self, fmt=''):
        """Formats a Version instance

        :param str fmt: the format spec (default is '', which means str(self));
                        parts can be:
                        M: the major part
                        m: the minor part
                        r: the release part

        Examples:
        >>> v = Version(1, 2, 5)
        >>> "{:M}".format(v)
        '1'
        >>> "{:m}".format(v)
        '2'
        >>> "{:r}".format(v)
        '5'
        >>> "{}".format(v)
        '1.2.5'
        """
        # See https://www.programiz.com/python-programming/methods/built-in/format
        # https://www.python.org/dev/peps/pep-3101/
        # print("format spec:", fmt)
        d = {'M': self.major,
             'm': self.minor,
             'r': self.release,
             }
        return "{}".format(d.get(fmt, str(self)))

    def __lt__(self, other):
        """self < other -> bool"""
        self._check_class(other)
        other = Version.fromsequence(other)
        return self.ver < other.ver

    def __le__(self, other):
        """self <= other -> bool"""
        self._check_class(other)
        other = Version.fromsequence(other)
        return self.ver <= other.ver

    def __gt__(self, other):
        """self > other -> bool"""
        self._check_class(other)
        return not self.ver < other.ver

    def __eq__(self, other):
        """self == other -> bool"""
        self._check_class(other)
        other = Version.fromsequence(other)
        return self.ver == other.ver

    def __ne__(self, other):
        """self != other -> bool"""
        self._check_class(other)
        other = Version.fromsequence(other)
        return not self.ver == other.ver

    def __add__(self, other):
        """self + other -> Version"""
        self._check_class(other)
        cls = self.__class__
        other = cls.fromsequence(other)
        version = [i+k for i, k in zip(self, other)]
        return cls(*version)

    def __iadd__(self, other):
        """self += other"""
        self._check_class(other)

        self.major += other.major
        self.minor += other.minor
        self.release += other.release
        return self


def _test():
    import doctest
    doctest.testmod(optionflags=doctest.IGNORE_EXCEPTION_DETAIL
                    | doctest.REPORT_UDIFF
                    | doctest.NORMALIZE_WHITESPACE
                    )


if __name__ == "__main__":
    _test()

    v1 = Version(1, 3)
    v2 = Version(1, 2, 5)
    v3 = Version(v1)
    v4 = Version(10, 2, 345)

    print("Version v1=", v1, repr(v1))
    print("Version v2=", v2)
    print("Version v3=", v3)
    print("bytes v2=", bytes(v2))
    print("from bytes:", Version.frombytes(bytes(v2)))
    print("v4:", v4)
    v4.typecode = 'I'
    print("from bytes:", bytes(v4), Version.frombytes(bytes(v4)))
    print("fromsequence:", Version.fromsequence((1, 3, 4)))
    print("fromstring:", Version.fromstring("1.43.23"))
    print("v1 + v2 =", v1 + v2)
    print("bump_major v1:", v1.bump_major())
    print("bump_minor v1:", v1.bump_minor())
    print("bump_release v1:", v1.bump_release())
    # print("v1 < v2", v1 < v2)
    # print("v1 < v3", v1 < v3)

    print("{:M}".format(v1))
    print(format(v1, "M"))

# EOF
