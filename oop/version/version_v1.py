#!/usr/bin/python3
# -*- coding: UTF-8 -*-
#
# Run the tests with:
# $ python3 -m doctest -v -o IGNORE_EXCEPTION_DETAIL -o REPORT_UDIFF -o NORMALIZE_WHITESPACE version_v1.py

"""
Test cases (some will fail):

# Initialisations

>>> Version()
Version(0, 0, 0)

# Comparisons

>>> Version(1,3) == Version(1,3,0)
True
>>> Version(1,2) < Version(1,2,1)
True
>>> Version(1,1,9) < Version(1,2)
True
>>> Version(1,1,1) > Version(1,0,1)
True
>>> Version(1,1,1) >= Version(1,0,1)
True
>>> Version(1,1,1) >= Version(1,1,1)
True
>>> Version(1,1,1) != Version(1,0,1)
True
>>> Version(1,1,1) != (1,0,1)
True
>>> Version(1,1,1) != [1,0,1]
True


# Formatting

>>> str(Version(1,1,1))
'1.1.1'
>>> repr(Version(1,1,1))
'Version(1, 1, 1)'
>>> eval(repr(Version(1,1,1))) == Version(1,1,1)
True
>>> tuple(Version(1,1,1))
(1, 1, 1)


# Calculation

>>> v1 = Version(1,1,1)
>>> v1 += Version(minor=2)
>>> v1
Version(1, 3, 1)
>>> v1 += Version(release=2)
>>> v1
Version(1, 3, 3)
>>> v1 += Version(major=1)
>>> v1
Version(2, 3, 3)
>>> v1.minor += 1
>>> v1
Version(2, 4, 3)
>>> v1 + Version(minor=1, release=1)
Version(2, 5, 4)
>>> v1.bump_major()
Version(3, 0, 0)
>>> v1.bump_minor()
Version(3, 1, 0)
>>> v1.bump_release()
Version(3, 1, 1)
>>> v1.bump_major().bump_release().bump_minor()
Version(4, 1, 0)

"""


class Version:
    """Stores and compares version strings"""

    def __init__(self, major=0, minor=0, release=0):
        """Stores and compares version strings

        Versions contains major, minor, and release numbers as integers.

        Versions can be constructed by using:

        - with no arguments like Version(), this is equivalent like
          Version(0,0,0)
        - with positional arguments like Version(release=4); this is equivalent
          like Version(0,0,4)
        - with another Version like Version(Version(1, 2, 3)); this is
          equivalent like Version(1, 2, 3)
        """
        self.major = major
        self.minor = minor
        self.release = release

    def bump_major(self):
        """Increase major version, set minor & release to zero"""
        self.major += 1
        self.minor, self.release = 0, 0
        return self

    def bump_minor(self):
        """Increase minor version, set release to zero"""
        self.minor += 1
        self.release = 0
        return self

    def bump_release(self):
        """Increase release version, leave major & minor untouched"""
        self.release += 1
        return self

    def __repr__(self):
        """repr(x) -> str"""
        return "{}({}, {}, {})".format(type(self).__name__,
                                       self.major,
                                       self.minor,
                                       self.release)

    def __str__(self):
        """str(self) -> str"""
        return "{}.{}.{}".format(self.major, self.minor, self.release)


if __name__ == "__main__":
    import doctest
    doctest.testmod(optionflags=doctest.IGNORE_EXCEPTION_DETAIL
                    | doctest.REPORT_UDIFF
                    | doctest.NORMALIZE_WHITESPACE
                    )

# EOF
