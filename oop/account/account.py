#!/usr/bin/env python3
#
# Run the tests with:
# $ python3 -m doctest -v account.py


"""

>>> a = Account("Tux", 42, 1000)
>>> b = Account("Konqine", 3423, 500)
>>> a.withdraw(1000)
0
>>> a.deposit(500)
500
>>> str(a)
'<Tux: 500>'
>>> repr(a)
'Account(Tux, 42, 500)'
>>> a += 500
>>> str(a)
'<Tux: 1000>'
>>> a -= 20
>>> str(a)
'<Tux: 980>'
>>> a + 50
Account(Tux, 42, 1030)
>>> a - 50  # HINT: a is still 980, not 1030
Account(Tux, 42, 930)
>>> a < 1000
True
>>> a <= 1000
True
>>> a == 1000
False
>>> a != 1000
True
>>> a > b
True
>>> a < b
False
>>> a >= b
True
>>> a <= b
False
>>> a == b
False
>>> a != b
True

>>> g1 = GiroAccount("Wilber", 3222, 500, limit=1000)
>>> str(g1)
'<Wilber: 500>'
>>> g2 = GiroAccount("Konqui", 4131, 20)
>>> g1.transfer(g2, 50)
450
>>> g1.transfer(g2, 4000)
Traceback (most recent call last):
...
ValueError: Can't transfer 4000 to 'Konqui' as it exeeds your limit of 1000
"""


class Account:
    """A base Account class to deposit and withdraw"""
    def __init__(self, owner, iban, balance=0):
        """Initialize an account

        :param str owner: the owner of this account
        :param str iban: the iban number of this account
        :param float balance: the current money
        """
        self.owner = owner
        self.iban = iban
        self.balance = balance

    def deposit(self, amount):
        """Deposit the "amount" into the account

        :param float amount: adds amount to this account
        :return: the new balance
        """
        self.balance += amount
        return self.balance

    def withdraw(self, amount):
        """Withdraws the "amount" from the account

        :param float amount: subtracts amount from this account
        :return: the new balance
        :rtype: float
        """
        self.balance -= amount
        return self.balance

    def __repr__(self):
        """repr(self)"""
        return "{}({}, {}, {})".format(self.__class__.__name__,
                                       self.owner,
                                       self.iban,
                                       self.balance)

    def __str__(self):
        """str(self)"""
        return "<{}: {}>".format(self.owner, self.balance)

    def __iadd__(self, amount):
        self.deposit(amount)
        return self

    def __add__(self, other):
        if not isinstance(other, (float, int)):
            raise TypeError("Cannot add type {}".format(type(other)))
        return Account(self.owner, self.iban, self.balance+other)

    def __isub__(self, amount):
        self.withdraw(amount)
        return self

    def __sub__(self, other):
        if not isinstance(other, (float, int)):
            raise TypeError("Cannot subtract type {}".format(type(other)))
        return Account(self.owner, self.iban, self.balance-other)

    def __lt__(self, other):
        """self < other"""
        if isinstance(other, type(self)):
            return self.balance < other.balance
        return self.balance < other

    def __gt__(self, other):
        """self > other"""
        return not self.__lt__(other)

    def __le__(self, other):
        """ self <= other"""
        if isinstance(other, type(self)):
            return self.balance <= other.balance
        return self.balance <= other

    def __ge__(self, other):
        """self >= other"""
        return not self.__le__(other)

    def __eq__(self, other):
        """self == other"""
        if isinstance(other, type(self)):
            return self.balance == other.balance
        return self.balance == other

    def __ne__(self, other):
        """self != other"""
        return not self.__eq__(other)


class GiroAccount(Account):
    """A giro account 
    """
    # debitinterest = 10/100   # Sollzinsen
    # creditinterest = 2.5/100 # Habenzinsen

    def __init__(self, owner, number, balance=0, limit=0):
        Account.__init__(self, owner, number, balance)
        self.limit = limit

    def transfer(self, account, amount):
        """Transfer the "amount" from this account to another
        
        :param Account account: the target account to transfer the money
        :param float amount: the amount to transfer
        :return: the new balance
        :rtype: float
        """
        if (self - amount) < -self.limit:
            raise ValueError("Can't transfer {a} to {o!r} "
                             "as it exeeds your limit of "
                             "{l}".format(a=amount,
                                          o=account.owner,
                                          l=self.limit)
                             )
        account.deposit(amount)
        return self.withdraw(amount)


if __name__ == "__main__":
    a1 = Account("Tux", 123456789, +1200)
    print(a1)
    a1 += 400
    print(a1)
    g1 = GiroAccount("Wilber", 3222, 500)
    print(g1)
