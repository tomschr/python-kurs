#!/usr/bin/python3
# -*- coding: UTF-8 -*-


class Time(object):
    """Time class to hold hours, minutes, and seconds
    """
    def __init__(self, hour=0, minute=0, second=0):
        assert 0 <= hour < 24
        assert 0 <= minute < 60
        assert 0 <= second < 60
        self._hour = hour
        self._min = minute
        self._sec = second

    # This is C++/Java-Style, not Python...
    def get_h(self):
        """Return hour (0-23)"""
        return self._hour
        
    def get_m(self):
        """Return minute (0-59)"""
        return self._min
    
    def get_s(self):
        """Return second (0-59)"""
        return self._sec
    
    # Provide alias names:
    get_hour=get_h
    get_minute=get_m
    get_second=get_s
        
    def __repr__(self):
        return "{Class}({self._hour}, {self._min}, {self._sec})".format(Class=self.__class__.__name__, self=self)


if __name__ == "__main__":    
    t1 = Time(11, 50)
    t2 = Time(11, 55)
    
    print("Time t1=>", t1)
    print("Time t2=>", t2)
    
    print("=== Using direct access ===")
    print("Hour of t1  =>", t1.get_h(), t1.get_hour() )
    print("Minute of t1=>", t1.get_m(), t1.get_minute() )
    print("Second of t1=>", t1.get_s(), t1.get_second() )
   
# EOF