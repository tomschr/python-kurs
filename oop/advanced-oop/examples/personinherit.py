#!/usr/bin/python3

from collections import namedtuple


class Person(namedtuple):
    # __slots__ = ()

    def __new__(cls, **kwargs):
        """

        :param cls:
        :param args:
        :param kwargs:
        :return:
        """

        return super().__new__(cls, **kwargs)

    def __repr__(self):
        """

        :return:
        """
        return "{0}{1!r}".format(self.__class__.__name__, self._asdict())


def _test():
    """Doctest """
    import doctest

    doctest.testmod(optionflags=doctest.IGNORE_EXCEPTION_DETAIL
                                | doctest.REPORT_UDIFF
                                | doctest.NORMALIZE_WHITESPACE
                    # | doctest.FAIL_FAST # Only in >=3.4
    )


if __name__ == "__main__":
    _test()

# EOF
