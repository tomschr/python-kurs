#!/usr/bin/python3
# -*- coding: UTF-8 -*-


class FuzzyMeta(type):
    def __new__(cls, name, bases, attrs):
        print("{}:", cls.__name__, )
        print("name={name}, bases={bases}, attrs={attrs}".format(**locals()))
        
        return super().__new__(cls, name, bases, attrs)

    def __init__(cls, *args, **kwargs):
        
        print("{cls.__name__}: args={args}, kwargs={kwargs}".format(**locals()))
        super().__init__(cls, args, kwargs)
        

class FuzzyBool(metaclass=FuzzyMeta):
    def __add__(self, other):
        raise NotImplementedError()
    
    
def _test():
    f1 = FuzzyBool()
    print("f1:", dir(f1))
    
if __name__=="__main__":
    _test()