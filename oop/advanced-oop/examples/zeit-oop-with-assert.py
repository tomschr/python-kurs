#!/usr/bin/python3
# -*- coding: UTF-8 -*-


class Time(object):
    """Time class to hold hours, minutes, and seconds
    """
    def __init__(self, hour=0, minute=0, second=0):
        assert 0 <= hour < 24
        assert 0 <= minute < 60
        assert 0 <= second < 60
        self._hour = hour
        self._min = minute
        self._sec = second

    def __repr__(self):
        return "{Class}({self._hour}, {self._min}, {self._sec})".format(Class=self.__class__.__name__, self=self)

if __name__ == "__main__":
    t1 = Time(11, 50)
    t2 = Time(11, 55)
    
    print("Time t1=>", t1)
    print("Time t2=>", t2)
    
# EOF