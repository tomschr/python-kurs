#!/usr/bin/python3
# -*- coding: UTF-8 -*-

"""
    This example contains a simple person class

    >>> p = Person("Tux", "Penguin")
    >>> p
    Person('Tux', 'Penguin')
    >>> p.first
    'Tux'
    >>> p.last
    'Penguin'

"""

class Person(object):
    """A class of a person"""
    
    # An "initializer" to setup the instance
    def __init__(self, firstname, lastname):
        # These are the 'public' attributes, accessible through its public API
        self.first = firstname
        self.last = lastname
    
    def __repr__(self):
        return "{name}('{self.first}', '{self.last}')".format(name=self.__class__.__name__, **locals())


if __name__ == "__main__":

    # Creating an instance of Person class (and save it to variable 'b'):
    #b = Person("Tux", "Penguin")
    
    # Displaying its content
    print(b)
    
    # We can also access its content through its public interface
    # This is done with the "dot" notation
    # being the ".first" and ".last" attributes
    print("Firstname:", b.first)
    print("Lastname: ", b.last)

# EOF