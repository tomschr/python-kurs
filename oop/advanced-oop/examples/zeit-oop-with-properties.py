#!/usr/bin/python3
# -*- coding: UTF-8 -*-


class Time(object):
    """Time class to hold hours, minutes, and seconds
    """
    def __init__(self, hour=0, minute=0, second=0):
        assert 0 <= hour < 24
        assert 0 <= minute < 60
        assert 0 <= second < 60
        self._hour = hour
        self._min = minute
        self._sec = second

    @property
    def h(self):
        """Return hour (0-23)"""
        return self._hour
        
    @property
    def m(self):
        """Return minute (0-59)"""
        return self._min
    
    @property
    def s(self):
        """Return second (0-59)"""
        return self._sec
    
    # Provide alias names:
    hour=h
    minute=m
    second=s
        
    def __repr__(self):
        return "{Class}({self.h}, {self.m})".format(Class=self.__class__.__name__, self=self)


if __name__ == "__main__":    
    t1 = Time(11, 50)
    t2 = Time(11, 55)
    
    print("Time t1=>", t1)
    print("Time t2=>", t2)
    
    print("=== Using direct access ===")
    print("Hour of t1  =>", t1.h, t1.hour)
    print("Minute of t1=>", t1.m, t1.minute)
    print("Second of t1=>", t1.s, t1.second)
   
# EOF