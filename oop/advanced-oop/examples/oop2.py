#!/usr/bin/python3
# -*- coding: UTF-8 -*-

"""
    This example contains a simple person class

    >>> p = Person("Tux", "Penguin")
    >>> p
    Person('Tux', 'Penguin')
    >>> p.first
    'Tux'
    >>> p.last
    'Penguin'

"""

class Person(object):
    """A class of a person"""    
    # An "initializer" to setup the instance
    def __init__(self, firstname, lastname):
        # These are the 'protected' attributes, accessible through its public API
        self._first = firstname
        self._last = lastname
    def __repr__(self):
        return "{Class}('{self._first}', '{self._last}')".format(Class=self.__class__.__name__, **locals())
    def __str__(self):
        return "{self._first} {self._last}".format(**locals())
    def sayhi(self):
        return "Hello, my name is {name}".format(name=str(self))

# This code is the same as the above:
# Person = type('Person', (object,), dict())
#
# def pinit(self, firstname, lastname): ...
# Person.__init__ = pinit
# def prepr(self): ...
# Person.__repr__ = prepr
# def sayhi(self): ...
# Person.sayhi = sayhi
        
if __name__ == "__main__":

    # Creating an instance of Person class (and save it to variable 'b'):
    b = Person("Tux", "Penguin")
    
    # Displaying its content
    print(repr(b)) # Person.__repr__(b)
    
    # We can also access its content through its public interface
    # This is done with the "dot" notation
    # being the ".first" and ".last" attributes
    #print("Firstname:", b.first)
    #print("Lastname: ", b.last)
    
    print(b.sayhi())

# EOF