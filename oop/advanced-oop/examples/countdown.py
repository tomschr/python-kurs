#!/usr/bin/python3


class CountdownX:
    def __init__(self, start=10, increment=1):
        self.start = start
        self.inc = increment

    # Forward iterator
    def __iter__(self):
        n = self.start
        while n >= 0:
            yield n
            n -= self.inc

    # Reverse iterator
    def __reversed__(self):
        n = 1
        while n <= self.start:
            yield n
            n += 1


# Another method to create countdowns
def Countdown(start=10, increment=1):
   x = start
   while x >=0:
        yield x
        x -= increment


if __name__ == "__main__":

   for i in CountdownX(increment=2):
      print(i)
