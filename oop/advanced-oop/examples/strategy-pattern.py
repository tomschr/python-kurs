#!/usr/bin/python3

import sys
from abc import ABCMeta, abstractmethod

class Strategy(metaclass=ABCMeta):
    @abstractmethod
    def execute(self, a, b):
        raise NotImplementedError()


class Add(Strategy):
    """
    >>> a = Add()
    >>> a.execute(2,2)
    4
    """
    def execute(self, a, b):
        """
        """
        # print("Called Add's execute()", file=sys.stderr)
        return a + b


class Context:
    __strategy=None
    
    def __init__(self, s):
        self.__strategy = s
    
    def execute(self, a, b):
        self.__strategy.execute(a, b)


def _test():
    """Doctest """
    import doctest
    doctest.testmod(optionflags=doctest.IGNORE_EXCEPTION_DETAIL
                                | doctest.REPORT_UDIFF
                                | doctest.NORMALIZE_WHITESPACE
                                # | doctest.FAIL_FAST # Only in >=3.4
                                )

    
if __name__ == "__main__":
    _test()
    A = Add()