#!/usr/bin/python3
# -*- coding: UTF-8 -*-

"""Account classes for storing owner and balance
"""


class BaseAccount:
    """Account class for storing owner and balance
    
    >>> tux = BaseAccount("Tux", 2000)
    >>> repr(tux)
    "BaseAccount('Tux', 2000)"
    >>> tux.balance
    2000

    >>> tux.withdraw(1000)
    BaseAccount('Tux', 1000)
    >>> tux.deposit(1000)
    BaseAccount('Tux', 2000)
    """
    
    def __init__(self, owner, money=0):
        """Constructor

        :param owner: the owner of this account (usually a string)
        :param money:  initalize the account class

        >>> tux = BaseAccount("Tux", -1000)
        >>> tux.b
        -1000
        >>> tux = BaseAccount("Tux")
        """
        self.owner = owner
        self.balance = money
        
    def __repr__(self):
        """x.__repr__() <==> repr(x)

        >>> tux = BaseAccount("Tux", 2000)
        >>> repr(tux)
        "BaseAccount('Tux', 2000)"
        """
        return '{}({!r}, {})'.format(self.__class__.__name__, self.__owner, self.balance)

    @property
    def balance(self):
        """Total money of this account
        
        >>> b=1000; tux = BaseAccount("Tux", b)
        >>> tux.balance == b
        True
        """
        return self.__total
        
    @balance.setter
    def balance(self, money):
        """Set total money

        :param money: Money to set
        :return:
        """
        self.__total = money
           
    # Alias for balance:
    b = balance
    # b = property(balance, balance.setter, doc="Alias for balance")

    @property
    def owner(self):
        """Get owner

        :return: string
        """
        return self.__owner

    @owner.setter
    def owner(self, name):
        """Set owner

        :param name: Name of the owner
        """
        self.__owner = name

        
    def transfer(self, account):
        """Transfer money from one account to another
        
        >>> tux = BaseAccount("Tux", 3000)
        >>> wilber = BaseAccount("Wilber", 10000)
        >>> wilber.balance
        10000
        >>> wilber.transfer(50)
        Traceback (most recent call last):
          ...
        TypeError: Cannot transfer <class 'int'>
        >>> wilber.transfer(tux)
        >>> tux.balance
        13000
        """
        if not isinstance(account, BaseAccount):
            raise TypeError("Cannot transfer {}".format(type(account)))
        account.balance += self.balance
        self.balance = 0
        
        
    # Magic methods
    def __iadd__(self, money):
        """ account += money <-> account = account + money <->  account.deposit(money)
        
        >>> tux = BaseAccount("Tux", 2000)
        >>> tux.balance
        2000
        >>> tux += 1000
        >>> tux.balance
        3000
        """
        self.balance = self.balance + money
        return self
        
    def __isub__(self, money):
        """account -= money <-> account = account - money <-> account.withdraw(money)

        :param money: money to withdraw
        :return: BaseAccount object

        >>> tux = BaseAccount("Tux", 2000)
        >>> tux.balance
        2000
        >>> tux -= 1000
        >>> tux.balance
        1000
        """
        self.balance -=  money
        return self
        
    # Aliases
    deposit = __iadd__
    withdraw = __isub__


class PositiveAccount(BaseAccount):
    """An Account class which only allows positive balances

    >>> tux = PositiveAccount("Tux")
    >>> tux.balance
    0
    >>> wilber = PositiveAccount("Wilber", -1000)
    Traceback (most recent call last):
      ...
    ValueError: Can only use/transfer positive money
    """

    @staticmethod
    def _checkbalance(money):
        """Checks, if money is positive

        :param money: Money to check for positive value
        """
        if money < 0:
            raise ValueError("Can only use/transfer positive money")


    def __init__(self, owner, money=0):
        """Constructor

        :param owner: the owner of this account (usually a string)
        :param money:  initalize the account class
        """
        PositiveAccount._checkbalance(money)
        super().__init__(owner, money)

    def __repr__(self):
        """x.__repr__() <==> repr(x)

        :return: string

        >>> tux = PositiveAccount("Tux")
        >>> repr(tux)
        "PositiveAccount('Tux', 0)"
        """
        return '{}({!r}, {})'.format(self.__class__.__name__, self.owner, self.balance)

    def transfer(self, account):
        """Transfer money from one account to another, but only if account balance > 0

        :param account:
        :return:
        """
        PositiveAccount._checkbalance(account.balance)
        super().transfer(account)

    def __isub__(self, money):
        """account -= money <=> account = account - money <=> account.withdraw(money)

        :param money: money to withdraw
        :return: PositiveAccount object

        >>> tux = PositiveAccount("Tux", 2000)
        >>> tux.balance
        2000
        >>> tux -= 3000
        Traceback (most recent call last):
          ...
        ValueError: Can only use/transfer positive money
        """
        PositiveAccount._checkbalance(self.balance - money)
        return super().__isub__(money)


def _test():
    """Doctest """
    import doctest
    doctest.testmod(optionflags=doctest.IGNORE_EXCEPTION_DETAIL
                                | doctest.REPORT_UDIFF
                                | doctest.NORMALIZE_WHITESPACE
                                # | doctest.FAIL_FAST # Only in >=3.4
                                )
    
if __name__ == "__main__": 
    _test()

# EOF
