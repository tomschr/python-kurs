#!/usr/bin/python

"""
currency classes



"""

class WrongCurrencyError(TypeError):
    pass


class Currency:
    """A currency class
    """
    def __init__(self, symbol):
        """Constructor

        :param symbol:

        >>> str(Currency('€'))
        '€'
        """
        self.__symbol = symbol

    def __repr__(self):
        return "{}({!r})".format(self.__class__.__name__, self.__symbol)

    def __str__(self):
        return "{}".format(self.__symbol)

# Create some currencies:
EUR=Currency("€")
USD=Currency("$")
NZD=Currency("NZ$")


class Money:
    """A money unit
    """
    def __init__(self, amount, currency):
        """Constructor

        :param amount:
        :param currency:
        :return:
        """
        self.amount = amount
        self.currency = currency

    @property
    def amount(self):
        return self.__amount

    @amount.setter
    def amount(self, v):
        self.__amount = v

    @property
    def currency(self):
        return self.__currency

    @currency.setter
    def currency(self, c):
        self.__currency = c

    def __repr__(self):
        """x.__repr__() <==> repr(x)

        :return: str
        """
        return "{}(amount={}, currency={!r})".format(self.__class__.__name__,
                                              self.amount, self.currency)

    def __str__(self):
        """x.__str__() <==> str(x)

        :return: str
        """
        return "{}{}".format(self.amount, str(self.currency))

    def _check(self, other):
        """Checks currency units

        :param other: a currency class
        :return: None
        """
        if not isinstance(other, Money):
            raise TypeError('Cannot add or subtract a ' +
                            'Money and non-Money instance.')
        if self.currency == other.currency:
            WrongCurrencyError("Wrong currency")

    def __add__(self, other):
        """x.__add__(y) <==> x+y

        :param other:
        :return: Currency
        """
        self._check(other)
        return self.__class__(amount=self.amount + other.amount,
                              #currency=self.currency
                             )

    def __sub__(self, other):
        """x.__sub__(y) <==> x-y

        :param other:
        :return:
        """
        self._check(other)
        return self.__class__(self.amount - other.amount)



class Euro(Money):
    """Euros

    >>> str(Euro(10))
    '10€'
    >>> repr(Euro(10))
    "Euro(amount=10, currency=Currency('€'))"
    >>> Euro(10) + Euro(5)
    Euro(amount=15, currency=Currency('€'))
    >>> Euro(10) - Euro(5)
    Euro(amount=5, currency=Currency('€'))

    >>> Euro(10) + NZDollar(10)
    """

    _currency = EUR

    def __init__(self, amount, currency=None):
        super().__init__(amount, Euro._currency)



class USDollar(Money):
    """US Dollar

    >>> repr(USDollar(5))
    "USDollar(amount=5, currency=Currency('$'))"
    """

    _currency = USD

    def __init__(self, value):
        super().__init__(value, USDollar._currency)

    def __str__(self):
        """x.__str__() <==> str(x)

        :return: str

        >>> str(USDollar(5))
        '$5'
        """
        return "{}{}".format(self.currency, self.amount)


class NZDollar(Money):
    """New Zealand Dollar


    >>> repr(NZDollar(100))
    "NZDollar(amount=100, currency=Currency('NZ$'))"
    """

    _currency= NZD

    def __init__(self, amount):
        super().__init__(amount, NZDollar._currency)

    def __str__(self):
        """x.__str__() <==> str(x)

        :return: str

        >>> str(NZDollar(12))
        'NZ$12'
        """
        return "{}{}".format(self.currency, self.amount)


def _test():
    """Doctest """
    import doctest
    doctest.testmod(optionflags=doctest.IGNORE_EXCEPTION_DETAIL
                                | doctest.REPORT_UDIFF
                                | doctest.NORMALIZE_WHITESPACE
                                # | doctest.FAIL_FAST # Only in >=3.4
                                )

if __name__ == "__main__":
    _test()

# EOF