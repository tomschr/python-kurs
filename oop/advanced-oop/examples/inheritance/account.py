#!/usr/bin/python3
# -*- coding: UTF-8 -*-

"""Account class for storing owner and balance
"""


class Account:
    """Account class for storing owner and balance
    
    >>> tux = Account("Tux", 2000)
    >>> repr(tux)
    'Account("Tux", 2000)'
    >>> tux.balance
    2000

    >>> tux.withdraw(1000)
    Account("Tux", 1000)
    >>> tux.withdraw(2000)
    Traceback (most recent call last):
    ValueError: Balance is <0

    >>> tux.deposit(1000)
    Account("Tux", 2000)
    """
    
    def __init__(self, owner, money=0):
        """Constructor

        - owner: the owner of this account (usually a string)
        - money: money to initalize the account class, must be positiv
        
        >>> tux = Account("Tux", -1000)
        Traceback (most recent call last):
        ValueError: ...
        >>> tux = Account("Tux")
        """
        self.__owner = owner
        if money < 0:
            raise ValueError("Can set account to negative value!")
        self.__total = money
        
    def __repr__(self):
        """ Account("owner", balance)

        >>> tux = Account("Tux", 2000)
        >>> repr(tux)
        'Account("Tux", 2000)'
        """
        return '{}("{}", {})'.format(self.__class__.__name__, self.__owner, self.balance)

    @property
    def balance(self):
        """Total money of this account (always positive)
        
        >>> b=1000; tux = Account("Tux", b)
        >>> tux.balance == b
        True
        """
        return self.__total
        
    @balance.setter
    def balance(self, money):
        """
        """
        if money >= 0:
            self.__total = money
        else:
            raise ValueError("Can't set negative Money!")
           
    # Alias for balance:
    b = balance

        
    def transfer(self, account):
        """Transfer money from one account to another
        
        >>> tux = Account("Tux", 3000)
        >>> wilber = Account("Wilber", 10000)
        >>> wilber.balance
        10000
        >>> wilber.transfer(50)
        Traceback (most recent call last):
        TypeError: Cannot transfer ...
        >>> wilber.transfer(tux)
        >>> tux.balance
        13000
        """
        if not isinstance(account, Account):
            raise TypeError("Cannot transfer {}".format(type(account)))
        account.balance += self.balance
        self.balance = 0
        
        
    # Magic methods
    def __iadd__(self, money):
        """ account += money <-> account = account + money <->  account.deposit(money)
        
        >>> tux = Account("Tux", 2000)
        >>> tux.balance
        2000
        >>> tux += 1000
        >>> tux.balance
        3000
        """
        self.balance = self.balance + money
        return self
        
    def __isub__(self, money):
        """acount -= money <-> account = account - money <-> account.withdraw(money)

        >>> tux = Account("Tux", 2000)
        >>> tux.balance
        2000
        >>> tux -= 1000
        >>> tux.balance
        1000
        """
        if (self.balance - money) < 0:
            raise ValueError("Balance is <0")
        self.balance -=  money
        return self
        
    # Alias
    deposit = __iadd__
    withdraw = __isub__

    
def _test():
    """Doctest """
    import doctest
    doctest.testmod(optionflags=doctest.IGNORE_EXCEPTION_DETAIL
                                | doctest.REPORT_UDIFF
                                | doctest.NORMALIZE_WHITESPACE
                                # | doctest.FAIL_FAST # Only in >=3.4
                                )
    
if __name__ == "__main__": 
    _test()

# EOF
