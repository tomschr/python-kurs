#!/usr/bin/python3

class PositiveInt(int):
   """A class which allows only positive integers

   >>> p1 = PositiveInt(4)
   >>> str(p1)
   '4'
   >>> PositiveInt(-2)
   Traceback (most recent call last):
     ...
   ValueError: ...
   >>> p2 = PositiveInt(5)
   >>> p1 + p2
   9
   >>> p3 = PositiveInt(2)
   >>> p1 - p3
   2
   >>> p1 - p2
   Traceback (most recent call last):
     ...
   ValueError: ...
   """
   __valueerrormsg="Only positive int's are allowd."

   # This is an immutable type. As such we have to use the static 
   # __new__ method to create our type.
   @staticmethod
   def __new__(cls, value):
      if value < 0:
         raise ValueError("{}. Got {}".format(cls.__valueerrormsg, value))
      return int.__new__(cls, value)

   def __isub__(self, other):
      if (self - other) < 0:
         raise ValueError("{}. Got {}".format(self.__valueerrormsg, other))
      self -= other

   def __sub__(self, other):
      if (self.numerator - other.numerator) < 0:
         raise ValueError("{}. Got {}".format(self.__valueerrormsg, other))
      return PositiveInt(self.numerator - other.numerator)

   def __neg__(self):
      raise ValueError(self.__valueerrormsg)


def _test():
    import doctest
    doctest.testmod(optionflags=doctest.IGNORE_EXCEPTION_DETAIL
                                | doctest.REPORT_UDIFF
                                | doctest.NORMALIZE_WHITESPACE
                                )

if __name__=="__main__":
   _test()

# EOF