#!/usr/bin/python3

import sys
from abc import ABCMeta, abstractmethod


class Person:
    """A Person class
    
    >>> p = Person("Tux", 20000101, "Grönland")
    >>> p.name
    'Tux'
    >>> p.birth
    20000101
    >>> p.address
    'Grönland'
    >>> p
    Person('Tux',20000101,'Grönland')
    """
    def __init__(self, name=None, birth=None, address=None):
        self.__name = name
        self.__birth = birth
        self.__address = address
        
    @property
    def name(self):
        """Gets the name value
        
        >>> Person("Wilber").name
        'Wilber'
        """
        return self.__name
    @name.setter
    def name(self, name):
        """Sets the name value
        
        >>> p = Person("Tux")
        >>> p.name
        'Tux'
        >>> p.name = "Wilber"
        >>> p.name
        'Wilber'        
        """
        self.__name = name
        
    @property
    def birth(self):
        return self.__birth
        
    @property
    def address(self):
        return self.__address
    @address.setter
    def address(self, address):
        self.__address = address
    
    def __repr__(self):
        return '{}({!r},{},{!r})'.format(self.__class__.__name__,
            self.name,
            self.birth,
            self.address,
            )
        
class Student(Person):
    """A Student class
    
    >>> p = Student("Tux", 20000101, "Grönland", "A B C", 5)
    >>> p.name
    'Tux'
    >>> p.birth
    20000101
    >>> p.address
    'Grönland'
    >>> p.classes
    'A B C'
    >>> p.grade
    5    
    """
    def __init__(self, name=None, birth=None, address=None, classes=None, grade=0):
        self.__classes = classes
        self.__grade = grade
        super().__init__(name, birth, address)
    
    @property
    def classes(self):
        """Get the classes value
        
        >>> Student(classes=10).classes
        10
        """
        return self.__classes
        
    @property
    def grade(self):
        """Gets the grade value
        
        >>> Student(grade=5).grade
        5
        """
        return self.__grade
        
    def __repr__(self):
        """
        """
        return '{}({!r},{},{!r},{!r},{!r})'.format(self.__class__.__name__,
            self.name,
            self.birth,
            self.address,
            self.classes,
            self.grade
            )



def _test():
    """Doctest """
    import doctest
    doctest.testmod(optionflags=doctest.IGNORE_EXCEPTION_DETAIL
                                | doctest.REPORT_UDIFF
                                | doctest.NORMALIZE_WHITESPACE
                                # | doctest.FAIL_FAST # Only in >=3.4
                                )

    
if __name__ == "__main__":
    _test()