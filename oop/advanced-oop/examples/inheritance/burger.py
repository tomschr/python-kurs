# -*- coding: UTF-8 -*-

WIDTH = 40


class Burger:
    def __init__(self, *content):
        self.content = content if content is not None else []
        self._width = 30

    def calories(self):
        return sum([i.calories() for i in self.content])

    def __repr__(self):
        res = ["Calories: {}".format(self.calories()),
               "/{}\\".format("-" * WIDTH)
        ]

        for i in self.content:
            res.append(repr(i))

        res.append("\\{}/".format("-" * WIDTH))
        # print(res)
        return "\n".join(res)


class Incredient:
    cals = None

    def calories(self):
        raise NotImplementedError

    def __repr__(self):
        t = "{} ({})".format(self.__class__.__name__, self.cals)
        return " |{0:^{1}}|".format(t, (WIDTH - 2))


class Cheese(Incredient):
    cals = 100  # per slice

    def __init__(self, slices=1):
        self.slices = slices

    def calories(self):
        return self.slices * self.cals


class Tomato(Incredient):
    cals = 20

    def __init__(self, slices=1):
        self.slices = slices

    def calories(self):
        return self.slices * self.cals


class Meat(Incredient):
    cals = 500

    def __init__(self, slices=1):
        self.slices = slices

    def calories(self):
        return self.slices * self.cals


class Salat(Incredient):
    cals = 10

    def __init__(self, slices=1):
        self.slices = slices

    def calories(self):
        return self.slices * self.cals


class TomsBurger(Burger):
    # width=50

    def __init__(self, name="Toms"):
        Burger.__init__(self, Meat(), Salat(), Tomato(10), Cheese(), Meat())  #
        self.name = name


if __name__ == "__main__":
    b1 = Burger(Meat(), Tomato(3), Cheese(), Meat())  #
    b2 = TomsBurger()

    print("Normaler Burger:")
    # print("Calories:", b1.calories() )
    print(b1)

    print("\nToms Burger:")
    # print("Calories:", b2.calories() )
    print(b2)

    # EOF