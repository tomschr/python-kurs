#!/usr/bin/python

from math import hypot


class Point:
    """A Point class

    """

    def __init__(self, x=0, y=0):
        """Point constructor

        :param x: X coordinate
        :param y: Y coordinate
        """
        self.x = x
        self.y = y

    def __repr__(self):
        """

        :return: str

        >>> repr(Point())
        'Point(x=0, y=0)'
        >>> repr(Point(x=1, y=2))
        'Point(x=1, y=2)'
        """
        return '{self.__class__.__name__}(' \
               'x={self.x}, ' \
               'y={self.y}' \
               ')'.format(self=self)

    def distancefromorigin(self):
        """Calculate the distance from origin (0,0)

        :return: float

        >>> p = Point(0, 0)
        >>> p.distancefromorigin()
        0.0
        """
        return hypot(self.x, self.y)

    def distance(self, other):
        """Calculates the distance from another Point class

        :param other: the other Point class
        :return: float

        >>> p1 = Point(1, 1)
        >>> p2 = Point(2, 2)
        >>> p1.distance(p2)
        1.4142135623730951
        >>> p2.distance(p1)
        1.4142135623730951
        """
        dx = other.x - self.x
        dy = other.y - self.y
        return hypot(dx, dy)


class Circle(Point):
    """A Circle class

    """

    def __init__(self, x=0, y=0, r=1):
        """Circle constructor

        :param x: X coordinate
        :param y: Y coordinate
        :param r: radius
        """
        if r < 0:
            raise ValueError("Radius cannot be <0!")
        self.r = r
        super().__init__(x, y)

    def __repr__(self):
        """x.__repr__(self) <=> repr(x)

        :return: str

        >>> repr(Circle())
        'Circle(x=0, y=0, r=1)'
        >>> repr(Circle(x=1, y=2, r=3))
        'Circle(x=1, y=2, r=3)'
        """
        return '{self.__class__.__name__}(' \
               'x={self.x}, ' \
               'y={self.y}, ' \
               'r={self.r}' \
               ')'.format(self=self)

    def distance(self, other):
        """Calculates the distance between two circles

        :type other: Circle
        :param other:
        :return: float

        >>> Circle(1, 1, 1).distance(Circle(4, 2, 1))
        1.1622776601683795
        >>> Circle(1, 1, 1).distance(Circle(4, 2, 3))
        -0.8377223398316205
        """
        d = super().distance(other)
        return d - self.r - other.r


def _test():
    """Doctest """
    import doctest

    doctest.testmod(optionflags=doctest.IGNORE_EXCEPTION_DETAIL
                                | doctest.REPORT_UDIFF
                                | doctest.NORMALIZE_WHITESPACE
                                # | doctest.FAIL_FAST # Only in >=3.4
                    )


if __name__ == "__main__":
    _test()

# EOF
