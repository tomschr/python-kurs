#!/usr/bin/python



class Date:
    """A date

    >>> d = Date(2014, 10, 12)
    >>> d
    Date(year=2014, month=10, day=12)
    """
    def __init__(self, year, month, day):
        """Date constructor

        :param year: the year
        :param month: the month
        :param day: the day
        """
        self.year = year
        self.month = month
        self.day = day

    def __str__(self):
        """

        :return: str
        """
        return '{self.year}/{self.month:02}/{self.day:02}'.format(self=self)

    def __repr__(self):
        """x.__repr__(self) <=> repr(x)

        :return: str
        """
        return '{self.__class__.__name__}' \
               '(year={self.year}, ' \
               'month={self.month}, ' \
               'day={self.day})'.format(self=self)


class EuroDate(Date):
    """European date class

    >>> e = EuroDate(2014, 10, 12)
    >>> str(e)
    '12.10.2014'
    >>> repr(e)
    'EuroDate(year=2014, month=10, day=12)'

    >>> e = EuroDate(2014, 3, 1)
    >>> str(e)
    '01.03.2014'
    """
    def __str__(self):
        """

        :return: str
        """
        return '{self.day:02}.{self.month:02}.{self.year}'.format(self=self)


def _test():
    """Doctest """
    import doctest
    doctest.testmod(optionflags=doctest.IGNORE_EXCEPTION_DETAIL
                                | doctest.REPORT_UDIFF
                                | doctest.NORMALIZE_WHITESPACE
                                # | doctest.FAIL_FAST # Only in >=3.4
                                )

if __name__ == "__main__":
    _test()

# EOF