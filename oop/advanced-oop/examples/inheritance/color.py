class Color:
    """Color class
    """
    def __init__(self, red, green, blue):
        self.red = red
        self.green = green
        self.blue = blue

    def check(self, value):
        """Checks, if the value is an integer and in the range (0...255)
        """
        if not isinstance(value, int):
            raise TypeError("Expected int, got %s" % type(value))
        if not 0 <= value < 256:
            raise ValueError("Color part cannot be %s. Must be [0, 255]" % value)
        return value

    @classmethod
    def from_int(cls, value):
        """Create a Color object from an int between (0...0xFFFFFF)
        """
        return cls(*convert_value2rgb(value))

    def convert_value2rgb(self, value):
        """Split value into red, green, blue parts"""
        if 0 <= value <= 0xffffff:
            raise ValueError("Can only be in the range (0..0xffffff), "
                             "but got 0x%X" % value)
        red = value & 0xFF0000
        green = value & 0x00FF00
        blue = value & 0x0000FF
        return (red, gree, blue)

    @classmethod
    def from_string(cls, string):
        """Create a Color object from a string. The string can be:
        * a hex-string: "0xFFaaBB"
        * a int-string: "16711850"
        * a tuple-string: "3,23,255"
        """
        def hexstring(string):
            try:
                rgb = int(string, 16)
            except ValueError:
                raise TypeError("Not a hexstring")
            return self.convert_value2rgb(rgb)

        def intstring(string):
            try:
                rgb = int(string)
            except ValueError:
                raise TypeError("No int string")
            return self.convert_value2rgb(rgb)

        def tuplestring(string):
            rgb = string.split(",")
            if len(rgb) != 3:
                raise TypeError("Not a tuple-string")
            rgb = tuple(int(v) for v in rgb)
            # Check, if any value is outside of (0...255):
            if not all(0 <= v <= 0xff for v in rgb):
                raise ValueError("One member of the tuple-string is "
                                 "outside of (0...255)")
            return rgb

        # flag = False

        for func in (hexstring, intstring, tuplestring):
            try:
                rgb = func(string)
                # flag = True
            except TypeError:
                pass
        return cls(*rgb)
        #raise ValueError('Expected a hex-string, int-string, or a tuple-string. '
        #                 'But neither one matched.')

    @classmethod
    def from_string(cls, value):
        """Allow '0xffffff' and '255,255,255'"""
        try:
            value = int(value, 16)
        except ValueError:
            # ...
            pass

    @classmethod
    def from_hsv(cls, hue, sat, val):
        """Create a color object from HSV (hue, saturation, and value)"""
        # https://de.wikipedia.org/wiki/HSV-Farbraum
        if sat == 0.0:
            val *= 255
            return cls(val, val, val)
        i = int(hue*6.) # XXX assume int() truncates!
        f = (hue*6.)-i
        p, q, t = int(255*(val*(1.-sat))), \
                  int(255*(val*(1.-sat*f))), \
                  int(255*(val*(1.-sat*(1.-f))))
        val *= 255
        i %= 6
        if i == 0: return cls(val, t, p)
        if i == 1: return cls(q, val, p)
        if i == 2: return cls(p, val, t)
        if i == 3: return cls(p, q, val)
        if i == 4: return cls(t, p, val)
        if i == 5: return cls(val, p, q)

    @property
    def red(self):
        return self._red
    @red.setter
    def red(self, red):
        self._red = self.check(red)

    @property
    def green(self):
        return self._green
    @green.setter
    def green(self, green):
        self._green = self.check(green)

    @property
    def blue(self):
        return self._blue
    @blue.setter
    def blue(self, blue):
        self._blue = self.check(blue)

    @property
    def rgb(self):
        return self.red, self.green, self.blue

    @property
    def hsv(self):
        # TODO:
        return None, None, None

    def __repr__(self):
        return "{}{}".format(type(self).__name__,
                             self.rgb)

    def __str__(self):
        return "{}".format(self.rgb)

    def __iter__(self):
        yield from self.rgb


if __name__ == "__main__":
    c = Color(1, 200, 240)
    print(c)
    print(repr(c))
    r, g, b = c
    print("r={}, g={}, b={}".format(r, g, b))
    #
    r = Color.from_hsv(359./360.,1,1)
    print(repr(r))
