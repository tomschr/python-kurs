#!/usr/bin/python3


class Animal:
    def __init__(self):
        #print("Init {}".format(self.__class__.__name__))
        super().__init__()
        print("Init Animal", type(self).__name__, self.__class__)

    def eat(self):
        print("Eat")

class Mammal(Animal):
    def __init__(self):
        #print("Init {}".format(self.__class__.__name__))
        super().__init__()
        print("Init Mammal", type(self).__name__, self.__class__)

    def breathe(self):
        print("Breath")

class WingedAnimal(Animal):
    def __init__(self):
        #print("Init {}".format(self.__class__.__name__))
        super().__init__()
        print("Init WingedAnimal", type(self).__name__, self.__class__)

    def flap(self):
        print("Flap")

class Bat(Mammal,WingedAnimal):
    def __init__(self):
        super().__init__()
        l=locals()
        print("Init {}".format(self.__class__.__name__))
        print("  ", l, l["__class__"])

if __name__ == "__main__":
    bat = Bat()
    bat.eat()
    #print(bat.eat)
    #print(dir(bat))