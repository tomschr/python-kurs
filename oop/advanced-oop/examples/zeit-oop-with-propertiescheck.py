#!/usr/bin/python3
# -*- coding: UTF-8 -*-


class Time(object):
    """Time class to hold hours, minutes, and seconds
    """
    def __init__(self, hour=0, minute=0, second=0):
        self._check(hour, minute, second)
        self._hour, self._min, self._sec = hour, minute, second

    def _check_hour(self, hour):
        """Checks for valid hour argument, otherwise raise TypeError"""
        if not 0 <= hour < 24:
            raise TypeError("Hour must be 0 <= hour < 24, but got {}".format(hour))
    
    def _check_minute(self, minute):
        """Checks for valid minute argument, otherwise raise TypeError"""
        if not 0 <= minute < 60:
            raise TypeError("Minute must be 0 <= hour < 60, but got {}".format(minute))
        
    def _check_second(self, second):
        """Checks for valid second argument, otherwise raise TypeError"""
        if not 0 <= second < 60:
            raise TypeError("Second must be 0 <= hour < 60, but got {}".format(second))
        
    def _check(self, hour, minute, second):
        """Performs consistency checks for hour, minute, and second arguments"""
        self._check_hour(hour)
        self._check_minute(minute)
        self._check_second(second)
        
    @property
    def h(self):
        """Return hour (0-23)"""
        return self._hour
    @h.setter
    def h(self, value):
        """Sets the hour"""
        self._check_hour(value)
        self._hour = value
        
    @property
    def m(self):
        """Return minute (0-59)"""
        return self._min
    @m.setter
    def m(self, value):
        """Sets the minute"""
        self._check_minute(value)
        self._min = value
    
    @property
    def s(self):
        """Return second (0-59)"""
        return self._sec
    @s.setter
    def s(self, value):
        """Sets the second"""
        self._check_second(value)
        self._sec = value
    
    # Provide alias names:
    hour=h
    minute=m
    second=s
        
    def __repr__(self):
        return "{Class}({self.h}, {self.m})".format(Class=self.__class__.__name__, self=self)


if __name__ == "__main__":    
    t1 = Time(11, 50)
    t2 = Time(11, 55)
    
    print("Time t1=>", t1)
    print("Time t2=>", t2)
    
    print("=== Using direct access ===")
    print("Hour of t1  =>", t1.h, t1.hour)
    print("Minute of t1=>", t1.m, t1.minute)
    print("Second of t1=>", t1.s, t1.second)
    
    print("=== Setting values with using properties ===")
    print("Before: Time t1=>", t1)
    t1.h = 23
    print("After:  Time t1=>", t1)
    
    try:
        print("Trying to set an invalid value for hours (using 60)...")
        t1.h = 60
    except TypeError as err:
        print("Error:", err)
   
# EOF