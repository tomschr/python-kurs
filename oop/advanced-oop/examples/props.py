#!/usr/bin/python3
# -*- coding: UTF-8 -*-

import sys
import builtins

def dump_args(func):
    "This decorator dumps out the arguments passed to a function before calling it"
    argnames = func.__code__.co_varnames[:func.__code__.co_argcount]
    fname = func.__code__.co_name

    def echo_func(*args,**kwargs):
        
        print(fname, ":", ', '.join('%s=%r' % entry for entry in zip(argnames,args) + list(kwargs.items())))
        return func(*args, **kwargs)

    return echo_func


def xproperty(function):
    keys = 'fget', 'fset', 'fdel'
    func_locals = {'doc':function.__doc__}
    def probe_func(frame, event, arg):
        if event == 'return':
            locals = frame.f_locals
            func_locals.update(dict((k, locals.get(k)) for k in keys))
            sys.settrace(None)
        return probe_func
    sys.settrace(probe_func)
    function()
    return builtins.property(**func_locals)


class Foo:
    def __init__(self):
        self._x = None
        self._ver = [0,0,0]
        # self.major, self.minor, self.release = 3*None

    def foo():
        doc = '''This is the doc string.'''
        
        def fget(self):
            return self._x

        def fset(self, value):
            self._x = value

        def fdel(self):
            raise NotImplementedError("Cannot delete")

        return property(**locals())
    foo = foo()
    
    
    @xproperty
    def major():
        def fget(self):
            return self._ver[0]
        def fset(self, value):
            if not isinstance(value, int):
                raise TypeError("Type does not match Foo")
            if value < 0:
                raise ValueError("Cannot assing value < 0")
            self._ver[0] = value
            
    
    @dump_args
    def bar(self, x=0, y=0):
        pass

class Bar:
    def __init__(self, x=0, y=0):
        self._x = x
        self._y = y
    
    def __repr__(self):
        return 'Bar(x="{}", y="{}")'.format(self._x, self._y)
        
    def __dir__(self):
        return ["x", "y", "foo"]
        
    
if __name__ == "__main__":
    #f = Foo()
    #print("foo=", f.foo)
    #f.foo = 200
    #print("foo=", f.foo)
    
    #print("dir=", dir(f))
    
    #print("major=", f.major)
    #f.major = 10
    #print("major=", f.major)
    ## f.major = -5
    
    #print(f.bar())
    
    #
    b = Bar()
    print("bar=", dir(b))
    
    