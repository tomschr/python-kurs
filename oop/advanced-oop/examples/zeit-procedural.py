#!/usr/bin/python3
# -*- coding: UTF-8 -*-


def time_sub( t1, t2 ):
    """Subtract two time values: t2 - t1"""
    assert len(t1)==2 and len(t2)==2
    
    t = [0,0]
    t[0] = t2[0] - t1[0]
    t[1] = t2[1] - t1[1]
    
    return t

def time_repr( t ):
    """Makes a time object human readable"""
    assert len(t)==2
    
    return "{0[0]}h {0[1]}min".format(t)

def time_check( t ):
    if not (0 <= t[0] < 24) and (0 <= t[1] < 60):
        return False
    else:
        return True

def time_check_with_exception( t ):
    if not 0 <= t[0] < 24:
        raise TypeError("Hour must be 0 <= hour < 24, but got {}".format(hour))
    if not 0 <= t[1] < 60:
        raise TypeError("Minute must be 0 <= hour < 60, but got {}".format(minute))

    
if __name__ == "__main__":
    # Daten:
    t1 = [ 11, 50 ]
    t2 = [ 11, 55 ]
    
    # Funktionen
    t = time_sub(t1,t2)
    print(t)
    print( time_repr(t) )
    