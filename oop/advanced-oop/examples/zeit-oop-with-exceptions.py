#!/usr/bin/python3
# -*- coding: UTF-8 -*-


class Time(object):

    def __init__(self, hour=0, minute=0, second=0):
        assert 0 <= hour < 24
        assert 0 <= minute < 60
        assert 0 <= second < 60
        self._hour = hour
        self._min = minute
        self._sec = second
        
        print(self)

    @property
    def h(self):
        """Return hour (0-23)"""
        return self._hour
        
    @property
    def m(self):
        """Return minute (0-59)"""
        return self._min
    
    @property
    def s(self):
        """Return second (0-59)"""
        return self._sec
    
    # Provide alias names:
    hour=h
    minute=m
    second=s
    
    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.h == other.h and self.m == other.m and self.s == other.s
        else:
            raise TypeError("Time object compared with {}".format(type(other)))
    
    def __repr__(self):
        return "{Class}({self.h}, {self.m})".format(Class=self.__class__.__name__, self=self)
        
    def __sub__(self, other):
        if isinstance(other, self.__class__):
            return Time(self.h - other.h, self.m - other.m, self.s - other.s)
        else:
            raise TypeError("Can't subtract Time object from type {}".format(type(other)))
        


if __name__ == "__main__":
    t1 = Time(11, 50)
    t2 = Time(11, 55)
    t3 = Time(11, 50)
    
    t = t2 - t1
    print("t2 - t1", t)
    print("Hour   t1=", t1.h)
    print("Hour   t2=", t2.h)
    print("Minute t1=", t1.minute)
    print("Minute t2=", t2.m)
    print("t1 == t2", t1 == t2)
    print("t1 == t3", t1 == t3)
    
    