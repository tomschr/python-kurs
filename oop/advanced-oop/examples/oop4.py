#!/usr/bin/python3
# -*- coding: UTF-8 -*-


class Base(object):
    """Base Class"""
    
    def __init__(self):
        print(">> Init Base")
        self.value = 10
    
    def __repr__(self):
        return "{name}({self.value})".format(name=self.__class__.__name__, **locals())

class Foo(Base):
    """Derived class """
    
    def __init__(self, foo):
        ## Use the following in Python2:
        #super(self.__class__, self).__init__()
        ## Use the following only for Python 3:
        super().__init__()
        
        print(">> Init Foo")
        self._foo = foo
    
    def __repr__(self):
        return "{name}({self._foo}) {self.value}".format(name=self.__class__.__name__, **locals())


if __name__ == "__main__":
    b = Base()
    print(b)
    
    f = Foo(23)
    print(f)

# EOF