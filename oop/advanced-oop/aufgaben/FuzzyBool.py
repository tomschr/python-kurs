#!/usr/bin/python3
# -*- coding: UTF-8 -*-
#


"""Ein Datentyp für Fuzzy Boolsche Algebra

Ziel:
  - Alle Tests müssen fehlerfrei durchlaufen (s.u.) :-)
  - Jede Methode muss dokumentiert sein
  - Fehlermeldungen sollen aussagefähig sein

Aufgaben:
  - Überlege welcher Datentyp intern verwendet werden sollte
  - Erzeuge einen Konstruktor; welcher Datentyp sollte erwartet werden?
  - 

Testcases:

>>> f = FuzzyBool()
>>> g = FuzzyBool(.5)
>>> h = FuzzyBool(3.75)
>>> f, g, h
(FuzzyBool(0.0), FuzzyBool(0.5), FuzzyBool(0.0))
>>> h = ~h
>>> print(f, g, h)
0.0 0.5 1.0
>>> f = FuzzyBool(0.2)
>>> f < g
True
>>> h >= g
True
>>> f + g
Traceback (most recent call last):
...
TypeError: unsupported operand type(s) for +: 'FuzzyBool' and 'FuzzyBool'
>>> int(h), int(g)
(1, 0)
>>> d = {f : 1, g : 2, h : 3}
>>> d[g]
2

"""

class FuzzyBool:
    """Ein Datentyp für Fuzzy Boolsche Algebra

       Der Datentyp bool kann nur die Werte True oder False enthalten. Im
       Gegensatz hierzu kann FuzzyBool auch "vielleicht wahr" oder "vielleicht
       falsch" ausgeben. 
       
       Das wird erreicht, indem Wahrheitswerte nur im Intervall [0.0, 1.0] 
       enthalten sind. Das heisst, ein Wert von 0.0 (bzw. 0%) bedeutet False,
       ein Wert von 1.0 (bzw. 100%) True. Alles dazwischen bedeuten anteilig
       True oder False, d.h. 0.5 bedeutet 50% True und 50% False.
       
       Es unterstützt die logischen Operatoren not (~), and (&) und or (|).
    """
    pass
    
    
    
def _test():
    import doctest
    doctest.testmod(optionflags=doctest.IGNORE_EXCEPTION_DETAIL
                                | doctest.REPORT_UDIFF
                                | doctest.NORMALIZE_WHITESPACE
                                )


if __name__=="__main__":
    _test()

# EOF