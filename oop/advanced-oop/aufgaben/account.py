#!/usr/bin/python3
# -*- coding: UTF-8 -*-

"""Eine Klasse für Bankkonten

Ziel:
  - Alle Tests müssen fehlerfrei durchlaufen :-)
  - Jede Methode muss dokumentiert sein
  - Fehlermeldungen sollen aussagefähig sein

Aufgaben:
* Implementierung des Konstruktors:
  - Erwartet einen "Geldwert" (z.B. Integer) und weist ihn dem Kontostand zu
  - Wird kein Geldwert angegeben, wird der Kontostand mit Null initalisiert
  - Negative Werte werfen eine ValueError Exception

* Verfügbare Methoden
  - withdraw(self, money): Abheben vom Konto
    Wenn der Kontostand durch das Abheben kleiner Null wird, soll dies durch
    einen ValueError Exception verhindert werden

  - deposit(self, money): Einzahlen auf das Konto
    Erhöht den Kontostand um den Betrag von money

* Datenstrukturen:
  - Account.balance
    Gegenwärtiger Kontostand; kann nie <0 sein
    
* Benötigte spezielle "Magic" Methoden
  - Account -= money: Alias für withdraw
  
  - Account += money: Alias für deposit


Tipps:
- Account.balance sollte als Property implementiert werden
- Versuche verschiedene Überprüfungen wiederzuverwenden


Testcases:

# Konstruktor:
>>> tux = Account("Tux", -1000)
Traceback (most recent call last):
ValueError: ...
>>> tux = Account("Tux")
>>> tux.balance
0
>>> tux = Account("Tux", 2000)
>>> repr(tux)
'Account("Tux", 2000)'
>>> tux.balance
2000

>>> tux.withdraw(1000)
Account("Tux", 1000)
>>> tux.withdraw(2000)
Traceback (most recent call last):
ValueError: ...

>>> tux.deposit(1000)
Account("Tux", 2000)

>>> tux.balance
2000
>>> tux += 1000
>>> tux.balance
3000

>>> wilber = Account("Wilber", 10000)
>>> wilber.balance
10000

>>> wilber.transfer(50)
Traceback (most recent call last):
TypeError: ...
>>> wilber.transfer(tux)
>>> tux.balance
13000
>>> wilber.balance
0
"""

class Account:
    """Account class for storing bank information (owner and balance)"""
    
    def __init__(self, owner, money): # Unvollständig
        """
        """
        pass
    
    # Hier Methoden angeben...

    
def _test():
    import doctest
    doctest.testmod(optionflags=doctest.IGNORE_EXCEPTION_DETAIL
                                | doctest.REPORT_UDIFF
                                | doctest.NORMALIZE_WHITESPACE
                                )
    

if __name__ == "__main__": 
    _test()

# EOF