#
#


import re


def check_integer(number: str) -> bool:
    """Checks, if the string is an integer between 0 and 256
    """
    try:
        # This just a test, if the first character is a number;
        # if it is something like "+" or "-", it fails with ValueError
        int(number[0])
        if 0 <= int(number) < 256:
            return True
        return False
    except ValueError:
        return False


def check_float(number: str) -> bool:
    """Checks, if the string is a float between 0.0 and 1.0 
    """
    try:
        float(number[0])
        if 0.0 <= float(number) <= 1.0:
            return True
        return False
    except ValueError:
        return False


def check_percentint(number: str) -> bool:
    """Checks, if the string is an percent integer between 0% and 100%
    """
    try:
        if number[-1] != "%":
            # No percent sign at the end means no percent number:
            return False
        # remove the percent sign:
        number = number[:-1]
        # Test, if the first character is a digit
        # int(number[0])
        if 0 <= int(number) <= 100:
            return True
        return False

    except ValueError:
        return False


def check_number(value: str) -> bool:
    """Checks if a string is an integer, a float, or a percent value

    :param value: a string which contains a number. The number has
                   the following conditions:

                   * an integer: 0 <= number < 256
                   * a float:  0.0 <= number 1.0
                   * a percent integer value: 0% <= number 100%
    :returns: a boolean which indicates, if the number is valid

    Examples:
    >>> check_number("10")
    True
    >>> check_number("0.1")
    True
    >>> check_number("45%")
    True
    """
    checks = (check_integer, check_float, check_percentint)

    # for check in checks:
    #    if check(value):
    #        return True
    # return False
    return any(check(value) for check in checks)


def validate_rgb_color(color: str) -> bool:
    """Validates a string with color information. The string has to be
       in this format:

         "red, green, blue"

       whereas red, green, and blue can be (including the boundaries):

       * an integer from 0...255
       * a float from 0.0...1.0
       * a percentage value from 0%...100%

       Examples:
       >>> validate_color("0,0,0")
       True
       >>> validate_color("255, 255, 255")
       True
       >>> validate_color("255,350,255")
       False
    """
    colors = color.split(",")
    if len(colors) < 3:
        return False
    return all(check_number(c.strip()) for c in colors)
