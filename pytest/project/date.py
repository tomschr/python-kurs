# coding=utf8
# the above tag defines encoding for this document and is for Python 2.x compatibility

import re



def checkdate(date):
    """
    """
    regex = r"^(\d|0\d|1\d|2\d|3[01])-(\d|0\d|1[012])-[12]\d{3}$"
    return bool(re.search(regex, date))
