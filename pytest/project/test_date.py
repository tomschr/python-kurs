import pytest

from date import checkdate


@pytest.mark.parametrize("date, expected", [
    ("01-02-2020", True),
    ("18-10-2020", True),
    ("23-12-2000", True),
    ("31-12-2000", True),
    ("32-12-2000", False),
    ("23-13-2000", False),
    ("22-11-4444", False),
])
def test_date(date, expected):
    checkdate(date) == expected
