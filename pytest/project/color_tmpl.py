import re


def validate_rgb_color(color: str) -> bool:
    """Validates a string with color information. The string has to be
       in this format:

         "red, green, blue"

       whereas red, green, and blue can be (including the boundaries):

       * an integer from 0...255
       * a float from 0.0...1.0
       * a percentage value from 0%...100%

       Examples:
       >>> validate_color("0,0,0")
       True
       >>> validate_color("255, 255, 255")
       True
       >>> validate_color("255,350,255")
       False
    """
