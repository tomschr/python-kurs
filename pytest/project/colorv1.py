import re


def check_number(value: str) -> bool:
    """Checks if a string is an integer, a float, or a percent value

    :param value: a string which contains a number. The number has
                   the following conditions:
                   * an integer: 0 <= number < 256
                   * a float:  0.0 <= number 1.0
                   * a percent integer value: 0% <= number 100%
    :returns: a boolean which indicates, if the number is valid

    Examples:
    >>> check_number("10")
    True
    >>> check_number("0.1")
    True
    >>> check_number("45%")
    True
    """
    # See also https://regex101.com/
    colorpercent = r"(?:100%|\d{1,2}%)"
    colorint = r"(?:\d|\d{2}|1\d{2}|2[0-4]\d|25[0-5])$"
    colorfloat = r"(?:0?\.[0-9]+|1\.0+)"
    #
    regex = rf"""^({colorpercent}
                  | {colorint}
                  | {colorfloat}
                 )$"""
    regex = re.compile(regex, re.VERBOSE)
    return bool(regex.match(value))


def validate_rgb_color(color: str) -> bool:
    """Validates a string with color information. The string has to be
       in this format:

         "red, green, blue"

       whereas red, green, and blue can be (including the boundaries):

       * an integer from 0...255
       * a float from 0.0...1.0
       * a percentage value from 0%...100%

       Examples:
       >>> validate_color("0,0,0")
       True
       >>> validate_color("255, 255, 255")
       True
       >>> validate_color("255,350,255")
       False
    """
    colors = color.split(",")
    if len(colors) < 3:
        return False
    return all(check_number(c.strip()) for c in colors)
