#
import pytest

from color import (validate_rgb_color,
                   check_number,
                   check_integer,
                   check_float,
                   check_percentint,
                   )


@pytest.mark.parametrize("value, expected", [
  # Test integers
  ("x", False),
  ("-0", False),
  ("0", True),
  ("10", True),
  ("100", True),
  ("255", True),
  ("256", False),
])
def test_check_integer(value, expected):
    assert check_integer(value) == expected


@pytest.mark.parametrize("value, expected", [
  # Test floats
  ("x", False),
  ("0.0", True),
  ("0.53", True),
  ("0.83343", True),
  ("1.0", True),
  ("2.0", False),
  ("-0.1", False),  
])
def test_check_float(value, expected):
    assert check_float(value) == expected


@pytest.mark.parametrize("value, expected", [
  # Test percent integers
  ("x", False),
  ("0%", True),
  ("10%", True),
  ("99%", True),
  ("100%", True),
  ("200%", False),
  ("0.5%", False),
])
def test_check_percentint(value, expected):
    assert check_percentint(value) == expected


@pytest.mark.parametrize("value, expected", [
  ("x", False),
  # Test integers
  ("-0", False),
  ("0", True),
  ("10", True),
  ("100", True),
  ("255", True),
  ("256", False),

  # Test floats
  ("0.0", True),
  ("0.53", True),
  ("0.83343", True),
  ("1.0", True),
  ("2.0", False),
  ("-0.1", False),

  # Test percent integers
  ("0%", True),
  ("10%", True),
  ("99%", True),
  ("100%", True),
  ("200%", False),
  ("0.5%", False),

])
def test_check_number(value, expected):
    assert check_number(value) == expected


@pytest.mark.parametrize("value, expected", [
  # Invalid input:
  ("x", False),
  # Test integers
  ("-0", False),
  ("0", False),
  # Test integers:
  ("100,100,100", True),
  # Test floats:
  ("0.0, 0.5, 0.85", True),
  ("1.0, 1.0, 1.0", True),
  # Test percent integers
  ("0%, 10%, 100%", True),
  ("100%, 0.5, 10", True),
])
def test_validate_rgb_color(value, expected):
    assert validate_rgb_color(value) == expected
