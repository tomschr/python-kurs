pytest-Ablaufplan
=================

Theorie
-------

1. Einstiegs-Fragen:
   a.) Was macht ihr wenn ein Programm nicht das tut was es soll?
       Vermutlich: debuggen, nach Fehler suchen etc.
   b.) Wie findet ihr den Fehler?
       * Debugger
       * Mit Kollegen sprechen
       * Liegenlassen
       * Code intensiv anschauen
       * Testing
   c.) Demonstration: KIWI-Testumgebung: 1368 Tests, ca. 108s (0:01:48)

2. Vorstellung von TDD
   TDD = Testgetriebene Entwicklung (engl. test-driven development)
   * Prinzip: Fokus auf Tests
   * Vorgang: Führt Tests aus; Test überprüfen die realen Bedingungen mit den
              erwarteten
   * Ziel: Jeder Test muss durchlaufen ("grün werden")
     => Macht Spaß und ist motivierend, wenn Tests "grün werden"
   * Vor- und Nachteile aufzeigen

3. Was für Typen von Tests gibt es?
   * Ebene 1: Unit Tests  <= in diesem Kurs
   * Ebene 2: Integration Tests
   * Ebene 3: System Tests
   * Ebene 4: Acceptance Tests

4. TDD in Python
   * Verschiedene Test-Frameworks:
     * Alte Methode: unittest (in stdlib verfügbar)
     * Komfortabler: pytest
     * Andere Libraries: nosetest, robot, behave, lettuce,
       https://www.lambdatest.com/blog/top-5-python-frameworks-for-test-automation-in-2019/

II. Praxis
----------

1. Vorbedingungen
   * Installieren des Pakets python3-pytest
   * Offen für eine neue Idee, weg von manuellen Testen, hin zum zielgerichtetem
     Schreiben von Tests

2. Vorstellung des Projekts:
   * ganz simples Test-Setup mit einem Verzeichnis:
     * eine Python-Datei mit Funktionen: projekt.py
     * eine Test-Datei: test-project.py
       Name ist Vorgabe (Standardeinstellung) von pytest, kann aber
       geändert werden falls gewünscht
   * => in diesem Verzeichnis werden wir uns die ganze Zeit befinden
   * Realistischerweise (und empfohlen) liegen Quellcode und Tests in
     verschiedenen Verzeichnissen; 
     => in diesem Kurs wollen wir uns nur auf das Testen fokusieren und
        ein kompliziertes Setup vermeiden.
   * Oder: virtuelle Python-Umgebung (python3 -m venv .env)

3. Aufrufen von pytest:
   $ pytest [-v]
   * Zeigen von weiteren möglichen Aufrufarten

4. Konfigurieren von pytest
   * Erstellen von pytest.ini, setzen der Optionen addopts und norecursedirs

5. Zyklus von TDD:
   * Test schreiben
   * Test-Suite ausführen (Test schlägt fehl)
   * Code implementieren
   * Alle Tests ausführen (Test sollte erfolgreich sein)
   * Refactoring
   * Wiederholen
   => in der Praxis nicht immer in dieser reinen Form möglich; manche
      Entwickler schreiben zuerst die Implementierung und dann den Test.
      Wichtig ist hier: Code + Tests sollten nach der Implementierung vorliegen,
      unwichtig in welcher Reihenfolge.

6. Was ist Coverage (Test-Abdeckung)?
   * Metrik (meist in Prozent) die anzeigt, wieviel Test-Code unseren
     Quellcode abdeckt (bzw. welche Stellen ungetestet sind)
   * Installieren des Pakets python3-pytest-cov
   * Optionen ergänzen in pytest.ini (addopts):
     --no-cov-on-fail, --cov=project, --cov-report=term-missing
   * Erneutes Ausführen von pytest und überprüfen der Ausgabe

7. Fortgeschrittenes pytest
   * Parametrisierungen / Fixtures
